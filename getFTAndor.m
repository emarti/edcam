function myimages = getFTAndor(nOfFrames, framesPerCCD, exposureTime, useTrigger, useShutter, ROIStartX, ROIStopX, ROIStartY, ROIStopY)
    

    % Mode
    AcquisitionMode = 4;% 1 = single scan, 2 = accumulate (sum scans), 3 = kinetics mode, 4 = fast kinetcs, 5 = run till abort
    [ret]=SetAcquisitionMode(AcquisitionMode);
    % Frame Transfer Specific
    [ret,xpixels,ypixels]=GetDetector;
    
%     [ret, numberspeeds] = GetNumberVSSpeeds;
%     for k =0:(numberspeeds-1)
%         [ret, speed] = GetVSSpeed(k);
%         [k, speed]
%         k = k+1;
%     end
    [ret] = SetVSSpeed(1);
    [ret] = SetFKVShiftSpeed(1); %doesn't do anything

    subareaheight = floor(1024/framesPerCCD);
    
    [ret] = SetFrameTransferMode(1); %not sure about the details of this functions...
%     [ret] = SetNumberKinetics(nOfFrames); %why doesn't this work??????
    [ret] = SetFastKinetics(subareaheight, framesPerCCD, double(exposureTime/1000), 4, 1, 1);
    
    % Shutter -- timings are wrong on internal mode
    ShutterType = 1;% TTL high
    if useShutter == 0
        ShutterMode = 2;% 0 = Auto, 2 = always closed
    else
%         ShutterMode = 0;% 0 = Auto, 2 = always closed
        ShutterMode = 1; %always open
    end
    ClosingTime = 100;% ms
    OpeningTime = 300;% ms
    [ret]=SetShutter(ShutterType,ShutterMode,ClosingTime,OpeningTime);

    % Trigger
    [ret]=SetTriggerMode(useTrigger);%0 = internal, 1 = external
    if (useTrigger)
        [ret] = SetFastExtTrigger(1); %maybe this will help
    end

%     [ret,xpixels,ypixels]=GetDetector;
%     if (x1 < 1) || (y1 < xpixels - subareaheight + 1) || (x2 > xpixels) || (y2 > ypixels) || (x1 > x2) || (y1 > y2)
%         x1 = 1; y1 = xpixels - subareaheight + 1; x2 = xpixels; y2 = ypixels;
%     end

    x1 = 1; x2 = xpixels;
%     y1 = ypixels-subareaheight+1; y2 = ypixels;
    y1 = 1; y2 = subareaheight;
    xpixels = x2 - x1 + 1;
    ypixels = y2 - y1 + 1;
    
%     if (binningFactor < 1) || (binningFactor > xpixels) || (binningFactor > ypixels)
        binningFactor = 1;
%     end
    [ret]=SetImage(binningFactor,binningFactor,x1,x2,y1,y2);
    data = zeros(xpixels*ypixels,framesPerCCD);

    % Take pictures
    data = zeros(xpixels*ypixels,framesPerCCD*ceil(nOfFrames/framesPerCCD));
    
    n = 1;
    abort = 0;
    for k=1:ceil(nOfFrames/framesPerCCD)
        if not(abort)
            [ret]=StartAcquisition;
            [ret,gstatus]=AndorGetStatus;
    %         while(gstatus ~= 20073)%DRV_IDLE
            while(gstatus == 20072)%DRV_IDLE
                pause(0.1);
                [ret,gstatus]=AndorGetStatus;
            end
            [ret, numstart, numend] = GetNumberNewImages()
            for m = numstart:numend
                [ret, data(:,n) ] = GetOldestImage(xpixels*ypixels);
                if ret == 20024 % no new data
                    abort = 1;
                else
                    n = n+1;
                end
            end
        end
    end
    
    
    % Shutter -- timings are wrong on internal mode
    ShutterType = 1;% TTL high
    if useShutter == 0
        ShutterMode = 2;% 0 = Auto, 2 = always closed
    else
%         ShutterMode = 0;% 0 = Auto, 2 = always closed
        ShutterMode = 0; %always open
    end
    
    [ret]=SetShutter(ShutterType,ShutterMode,ClosingTime,OpeningTime);

    if abort
        myimages = 0;
    else
        myimages = zeros(ypixels,xpixels,nOfFrames);
        for k = 1:nOfFrames
            myimages(:,:,k) = transpose(reshape(data(:,k), xpixels,ypixels));
        end
    end

%     picBuff = squeeze(reshape(data,size(data,1)*size(data,2)*size(data,3),1,1));
    
%     %Take background
%     if not(takebackground == 0)
%         [ret]=SetShutter(ShutterType,2,ClosingTime,OpeningTime);
%         [ret]=SetNumberKinetics(1);
%         [ret]=StartAcquisition;
%         [ret,gstatus]=AndorGetStatus;
%         while(gstatus ~= 20073)%DRV_IDLE
%             pause(0.5);
%             [ret,gstatus]=AndorGetStatus;
%         end
%         numend = numend + 1;
%         [ret, data(:,numend) ] = GetOldestImage(xpixels*ypixels);
%         [ret]=SetShutter(ShutterType,ShutterMode,ClosingTime,OpeningTime);
%     end
    

    
