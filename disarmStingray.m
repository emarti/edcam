function disarmStingray()
    cameras = imaqfind;
    if ~isempty(cameras)
        stingray = cameras(1);
        delete(stingray);
        clear stingray;
    end
    imaqreset;