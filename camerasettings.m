%Just copy and paste this entry over and over. Put the filesnames (like
%'andor.mat') into edcam: popupmenu_camera_Callback

    edcamroot = [ matlabroot '\toolbox\edcam\current\' ];
    
    %Andor iKon
    camera = struct;
    camera.CCD = [1024 1024]; % camera specific!!!
    camera.type = 'Andor iKon';
    camera.take = 'FT';
    
    camera.roirect = [ 1 camera.CCD(2) 1 camera.CCD(1) ];
    
    camera.exposuretime = 0.2;
    camera.iscameraready = 0;
    camera.externaltrigger = 1;
    camera.continuous = 1;
    
    camera.pixelsize = 13/4.21; % camera specific!!! magnification * pixel size
    camera.efficiency = 0.838; % 0.85  camera specific!!! quantum efficieny = losses from cell*quantum efficiency of the CCD
    camera.gain = 0.952; % A/D counts per electron
    camera.crosssection = 0.2907*1; %0.2907 microns^2
    camera.detuning = 0;
    camera.linewidth = 38.11; % 2*pi*6.065 megahertz
    camera.scatteringrate = 38.11/2;
    camera.pulselength = 50; % microseconds
    camera.solidangle = pi*0.12^2; % approximately pi*NA^2
    save([ edcamroot 'ikon.mat'],'camera'); 

    
    %Andor iXon
    camera = struct;
    camera.CCD = [1004 1002]; % camera specific!!!
    camera.type = 'Andor iXon';
    camera.take = 'OD with dark';
    
    camera.roirect = [ 1 camera.CCD(2) 1 camera.CCD(1) ];
%     camera.roirect = [ 151 650 301 800 ];
    
    camera.exposuretime = 0.5; %in ms
    camera.iscameraready = 0; %default: leave at 0
    camera.externaltrigger = 1;
    camera.continuous = 1;
    
    camera.pixelsize = 2.04; % see USAF test pattern 1951 data from 2012-01-05 % camera specific!!! magnification * pixel size
    camera.efficiency = .177; % camera specific!!! quantum efficieny = losses from cell*quantum efficiency of the CCD
    camera.gain = 1; % A/D counts per electron
    camera.crosssection = 0.2907*1; %0.2907 microns^2
    camera.detuning = 0;
    camera.linewidth = 38.11; % 2*pi*6.065 megahertz
    camera.scatteringrate = 38.11/2;
    camera.pulselength = 50; % microseconds; 0 = low intensity (s << 1)
    camera.solidangle = pi*0.1^2; % approximately pi*NA^2
    save([ edcamroot 'ixon.mat'],'camera'); 

    %Guppy
    camera = struct;
    camera.CCD = [244 384]; % camera specific!!!
    camera.type = 'Guppy';
    camera.take = 'fluorescence';
    
    camera.roirect = [ 1 camera.CCD(2) 1 camera.CCD(1) ];
    
    camera.exposuretime = 0.1;
    camera.iscameraready = 0;
    camera.externaltrigger = 1;
    camera.continuous = 1;
    
    camera.pixelsize = 56.5; % camera specific!!! magnification * pixel size
    camera.efficiency = 0.59; % minimum gain quantum efficieny = losses from cell*quantum efficiency of the CCD
    camera.gain = 10^(-2.4); % A/D counts per electron
    camera.crosssection = 0.215*1; %0.2907 microns^2
    camera.detuning = 4.43;
    camera.linewidth = 36.88; % 2*pi*6.065 megahertz
    camera.scatteringrate = 1.8;
    camera.pulselength = 100; % microseconds
    camera.solidangle = (18)^2/(4*230^2); % approixmately pi*NA^2
    save([ edcamroot 'guppy.mat'],'camera'); 
    
    %Stingray
    camera = struct;
    camera.CCD = [290 388]; % camera specific!!!
    camera.type = 'Stingray';
    camera.take = 'OD with dark';
    
    camera.roirect = [ 1 camera.CCD(2) 1 camera.CCD(1) ];
    
    camera.exposuretime = 0.5;
    camera.iscameraready = 0;
    camera.externaltrigger = 1;
    camera.continuous = 1;
    
    camera.pixelsize = 8.3*2*3.17; % camera specific!!! magnification * pixel size
    camera.efficiency = 0.35; % minimum gain quantum efficieny = losses from cell*quantum efficiency of the CCD
    camera.gain = 0.30; %10^(-2.44); % A/D counts per electron
    camera.crosssection = 0.2907*1; %0.2907 microns^2
    camera.detuning = 0;
    camera.linewidth = 2*pi*6.06539; % 2*pi*6.065 megahertz
    camera.scatteringrate = 19.06;
    camera.pulselength = 0; % microseconds
    camera.solidangle = pi*(12)^2/(300^2); % approximately pi*NA^2. Why was there a 4 on the bottom?
    save([ edcamroot 'stingray.mat'],'camera'); 


    
    % Li cross-section: 0.215 for sigma+, 0.100 for randomly polarized
    % (random = maximum*7/15)
    % Rb cross-section: 0.2907 for sigma+, 0.1356 for randomly polarized
    % (Steck) or 0.12 (Lewandowski)