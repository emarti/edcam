runs = 1E3;
avg = 1000;
y=pnoise(avg + zeros(1,runs));
% y = randn(1,runs)*10+100;
x=1:30;

yexp = runs*(avg.^x).*exp(-avg)./factorial(x);

sprintf('integrated variance (ideal): %f',sum((x-avg).^2.*yexp)/sum(yexp))

ybinned = histc(y,x);

sprintf('integrated variance (binned): %f',sum((x-avg).^2.*ybinned)/sum(ybinned))
sprintf('integrated mean (binned): %f',sum(x.*ybinned)/sum(ybinned))

plot(x,yexp,x,ybinned)





subplot(2,1,1)
hist(y,x)
subplot(2,1,2)
plot(y)
% hold on
% plot(x,yexp)
% hold off
% % plot(y)
sprintf('variance(1): %f',sum((y-mean(y)).^2)./(runs-1))
sprintf('variance(2): %f',var(y))
sprintf('std^2: %f',std(y)^2)
sprintf('std: %f',std(y))
sprintf('mean: %f',mean(y))