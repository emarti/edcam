%This function takes a large cell of structures and combines them into a
%single, multi-dimensional structure. If some of the structures in the cell
%are missing entries, combinestructs will add these entries and assign
%blanks to their values ([])
function newstruct = horzcombinestructs(cellofstructs)

%find all the field names, retaining some sort of order
fn = fieldnames(cellofstructs{1});
n = 0;
for i = 2:length(cellofstructs)
        fn = [ fn; setdiff(fieldnames(cellofstructs{i}),fn) ];
end

%add the names to all the structs missing them
for k = 1:length(cellofstructs)
    for m = 1:length(fn)
        if not(isfield(cellofstructs{k},fn(m)))
            for n = 1:size(cellofstructs{k})
              cellofstructs{k}(n).(fn{m}) = [];
            end
        end
    end
end

%combine the structs -- shouldn't grow but oh well
newstruct = struct(cellofstructs{1});
for k = 2:length(cellofstructs)
    newstruct = [newstruct cellofstructs{k}];
end

%clean up
for k=1:length(fn)
    if isequal(newstruct.(fn{k}),[])
        newstruct = rmfield(newstruct,fn(k));
    end
end
