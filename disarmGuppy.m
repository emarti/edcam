function disarmGuppy()
    cameras = imaqfind;
    guppy = cameras(1);
    delete(guppy);
    clear guppy;
    imaqreset;