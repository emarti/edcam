function varargout = atomic(varargin)
% ATOMIC M-file for atomic.fig
%      ATOMIC, by itself, creates a new ATOMIC or raises the existing
%      singleton*.
%
%      H = ATOMIC returns the handle to a new ATOMIC or the handle to
%      the existing singleton*.
%
%      ATOMIC('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ATOMIC.M with the given input arguments.
%
%      ATOMIC('Property','Value',...) creates a new ATOMIC or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before atomic_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to atomic_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help atomic

% Last Modified by GUIDE v2.5 05-Dec-2008 08:37:48

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @atomic_OpeningFcn, ...
                   'gui_OutputFcn',  @atomic_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



% --- Executes just before atomic is made visible.
function atomic_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to atomic (see VARARGIN)

% Choose default command line output for atomic
handles.output = hObject;

global mydata
mydata = varargin{1};

% mydata = struct;
% mydata(1).tag = 'Rb MOT';
% mydata(1).crosssection = .1;
% 
% mydata(2).tag = 'Rb';
% mydata(2).crosssection = .2;

global currentval
currentval = 1;

global currentdata
currentdata = mydata(currentval);

% 
% set(handles.popupmenu_tag,'String',{ mydata.tag })
populate(handles,mydata,currentval);


% Update handles structure
% handles.output = mydata;
guidata(hObject, handles);

% UIWAIT makes atomic wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = atomic_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
global mydata
varargout{1} = mydata;

% vargout{1} = mydata;


% --- Executes on selection change in popupmenu_tag.
function popupmenu_tag_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu_tag contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_tag
global mydata currentdata currentval
% contents = get(hObject,'String');
% currenttag = contents{get(hObject,'Value'),1};
currentdata = mydata(get(hObject,'Value'));
currentval = get(hObject,'Value');

populate(handles,mydata,currentval);

% --- Executes during object creation, after setting all properties.
function popupmenu_tag_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_tag_Callback(hObject, eventdata, handles)
% hObject    handle to edit_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_tag as text
%        str2double(get(hObject,'String')) returns contents of edit_tag as a double
global currentdata
    currentdata.tag = get(hObject,'String');


% --- Executes during object creation, after setting all properties.
function edit_tag_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_new.
function pushbutton_new_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_new (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global mydata currentdata currentval
currentval = length(mydata)+1;
mydata(currentval) = currentdata;
populate(handles,mydata,currentval);

% --- Executes on button press in pushbutton_save.
function pushbutton_save_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global mydata currentdata currentval
mydata(currentval) = currentdata;
populate(handles,mydata,currentval);

% --- Executes on button press in pushbutton_delete.
function pushbutton_delete_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_delete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)





function edit_crosssection_Callback(hObject, eventdata, handles)
% hObject    handle to edit_crosssection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_crosssection as text
%        str2double(get(hObject,'String')) returns contents of edit_crosssection as a double
global currentdata
    currentdata.crosssection = str2double(get(hObject,'String'));


% --- Executes during object creation, after setting all properties.
function edit_crosssection_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_crosssection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function populate(handles,data,val)
set(handles.popupmenu_tag,'String',{ data.tag });
set(handles.popupmenu_tag,'Value',val);
set(handles.edit_tag,'String',data(val).tag);
set(handles.edit_crosssection,'String',num2str(data(val).crosssection));


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
uiresume(hObject);
delete(hObject);




% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

