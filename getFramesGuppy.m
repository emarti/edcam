function myimages = getFramesGuppy(nOfFrames, exposureTime, useTrigger, useShutter, ROIStartX, ROIStopX, ROIStartY, ROIStopY)
    ROIStartX = 1; ROIStopX = 768;
    ROIStartY = 1; ROIStopY = 388;
    xpixels = ROIStopX - ROIStartY + 1;
    ypixels = ROIStopY - ROIStartY + 1;

%     imaqreset;
    cameras = imaqfind;
    guppy = cameras(1);
    src = getselectedsource(guppy);
    
    set(guppy,'TriggerRepeat',nOfFrames-1);
    
    shutter = round((exposureTime - 0.042)/0.02);
    if shutter < 1
        shutter = 1;
    end
    if shutter > 4095
        shutter = 4095;
    end
    set(src,'Shutter',shutter);
    if useTrigger
        triggerconfig(guppy,'hardware','fallingEdge','externalTrigger');
    else
        triggerconfig(guppy,'immediate','none','none');
    end
    
    start(guppy);
%     'waiting'
    wait(guppy);

%     myimages = zeros(ypixels,xpixels,nOfFrames);
    if (get(guppy,'FramesAvailable') == nOfFrames)
        for i=1:nOfFrames
            [ myimages(:,:,i) timeData metaData ] = getdata(guppy);
%             myimages(:,:,i) = getdata(guppy);
            datestr(metaData.AbsTime)
            datestr(metaData.AbsTime,'FFF')
        end
        'got frames!'
    else
        myimages = 0;
        'nope, nothing'
    end
    
    if strcmp(get(guppy,'VideoFormat'),'F7_Y8_768x492')
        myimages = deinterlace(myimages);
    end

    clear guppy;

%     imaqmem;ans.FrameMemoryUsed
    
    
    % Fix Look-Up Table <-- DOUBLE-CHECK THIS!!!
    
%     myimages=double(uint16(myimages).^2);
    
    