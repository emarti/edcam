function varargout = editstruct(varargin)
% EDITSTRUCT lets you view and edit multidimensional structures. The
% structure must have a field 'name' which is used for the popup menu. This
% program assumes the entries are numeric.
    switch nargin
        case 0
            data = struct;
            currentval = 1;
        case 1
            originaldata = varargin{1};
            data = originaldata;
            currentval = 1;
        case 2
            originaldata = varargin{1};
            data = originaldata;
            currentval = varargin{2};
    end
    
    if currentval > length(data)
        currentval = length(data);
    end
    
    if not(isfield(data,'name'))
        [data.name] = deal('');
        [originaldata.name] = deal('');
    end
    fntemp = fieldnames(data);
    fn = fntemp;
    fn{1} = 'name';
    p = 1;
    while not(strcmp(fntemp(p),'name')) && p <= length(fn)
        p = p+1;
        fn{p} = fntemp{p-1};
    end
    data = orderfields(data,fn);
    
    xoffset = 25;
    yoffset = 8;
    textwidth = 150;
    textheight = 15;
    editheight = 18;
    popupheight = 20;
    buttonwidth = 50;
    buttonheight = 25;
    totwidth = xoffset*3+textwidth+buttonwidth;
    totheight = length(fn)*(yoffset+editheight+textheight)+popupheight-textheight+yoffset;
    fontsize = 8;
    

    f = figure('Visible','on','Position',[500 400 totwidth totheight],'WindowStyle','modal',...
        'CloseRequestFcn',{@OutputFcn});
    
    hpopup = uicontrol('Style','popupmenu','String',{ data.name },...
        'Callback',{@popupCallback},'Position',[xoffset totheight-yoffset-popupheight textwidth popupheight],...
        'FontSize',fontsize);
    
    for i=1:length(fn)
        y = (length(fn)-i)*(yoffset+editheight+textheight)+yoffset;
        if i>1
        htext(i) = uicontrol('Style',    'text',...
                             'String',   fn(i),...
                             'Position', [xoffset y+textheight textwidth textheight],...
                             'HorizontalAlignment','left',...
                             'FontSize',fontsize,...
                             'BackgroundColor',get(f,'Color'));
        end
        
        hedit(i) = uicontrol('Style',    'edit',...
                             'Position', [xoffset y textwidth editheight],...
                             'HorizontalAlignment','left',...
                             'Callback',{@editCallback,i},...
                             'FontSize',fontsize,...
                             'BackgroundColor','white');
    end
    
    hnew = uicontrol('Style',    'pushbutton',...
                     'String',   'New',...
                     'Position', [xoffset*2+textwidth totheight-yoffset-buttonheight ...
                                  buttonwidth buttonheight],...
                     'Callback',{@newCallback},...
                     'FontSize',fontsize);

    hdelete = uicontrol('Style',    'pushbutton',...
                        'String',   'Delete',...
                        'Position', [xoffset*2+textwidth totheight-yoffset-2*buttonheight ...
                                     buttonwidth buttonheight],...
                        'Callback',{@deleteCallback},...
                        'FontSize',fontsize);
    
    populate();
    output = originaldata;
    uiwait(f);
    
    function OutputFcn(source,eventdata)
        output = orderfields(data,originaldata);
        varargout{1} = output;
        varargout{2} = currentval;
        delete(gcf);
    end

    function editCallback(source,eventdata,myfield)
        newentry = get(hedit(myfield),'String');
        if strcmp(fn{myfield},'name');
            data(currentval).(fn{myfield}) = newentry;
        else
            temp = str2double(newentry);
            if not(isnan(temp))
                data(currentval).(fn{myfield}) = temp;
            end            
        end
        populate();
    end

    function popupCallback(source,eventdata)
        currentval = get(hpopup,'Value');
        populate();
    end

    function newCallback(source,eventdata)
        oldval = currentval;
        currentval = length(data)+1;
        data(currentval) = data(oldval);
        data(currentval).name = '';
        populate();
    end
    
    function deleteCallback(source,eventdata)
        if length(data) > 1
            data(currentval) = [];
            currentval = currentval - 1;
        end
        populate();
    end

    function populate()
        if currentval > length(data)
            currentval = length(data);
        end
        if currentval < 1
            currentval = 1;
        end
        set(hpopup,'String',{ data.name });
        set(hpopup,'Value',currentval);
        for j=1:length(fn)
            set(hedit(j),'String',data(currentval).(fn{j}));
        end
    end
end