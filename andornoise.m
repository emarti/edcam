function [speed1a speed1b speed2 speed3] = andornoise()
    % 50 kHz
    [ret]=SetADChannel(0); % 0: 0.05 MHz; 1: 1.0 or 2.5 MHz
    [ret]=SetHSSpeed(0,0); % 0,0: 0.05 MHz or 2.5 MHz; 0,1: 1.0 MHz
    pause(60);
    speed1a = GetNFramesAndor(200,0,0,0,1,100,1,100);
    subplot(4,1,1)
    plot(std(reshape(speed1a,100*100,200)))
    
    [ret]=SetADChannel(0); % 0: 0.05 MHz; 1: 1.0 or 2.5 MHz
    [ret]=SetHSSpeed(0,0); % 0,0: 0.05 MHz or 2.5 MHz; 0,1: 1.0 MHz
    pause(60);
    speed1b = GetNFramesAndor(200,0,0,0,1,100,1,100);
    subplot(4,1,2)
    plot(std(reshape(speed1b,100*100,200)))

    
    % 1 MHz
    [ret]=SetADChannel(1); % 0: 0.05 MHz; 1: 1.0 or 2.5 MHz    
    [ret]=SetHSSpeed(0,1); % 0,0: 0.05 MHz or 2.5 MHz; 0,1: 1.0 MHz
    pause(60);
    speed2 = GetNFramesAndor(200,0,0,0,1,100,1,100);
    subplot(4,1,3)
    plot(std(reshape(speed2,100*100,200)))
    
    % 2.5 MHz
    [ret]=SetADChannel(1); % 0: 0.05 MHz; 1: 1.0 or 2.5 MHz
    [ret]=SetHSSpeed(0,0); % 0,0: 0.05 MHz or 2.5 MHz; 0,1: 1.0 MHz
    pause(60)
    speed3 = GetNFramesAndor(200,0,0,0,1,100,1,100);
    subplot(4,1,4)
    plot(std(reshape(speed3,100*100,200)))
    