% We assume that the structs all have different entries. If a struct is
% multi-dimensional, it is turned into a one-dimensional structure whose
% fieldnames are the original ones plus a number, i.e. NSum ->
% NSum1,NSum2,etc.
function newstruct = vertcombinestructs(cellofstructs)
newstruct = struct;
for i=1:length(cellofstructs)
    fn = fieldnames(cellofstructs{i});
    for j=1:length(cellofstructs{i})
        if length(cellofstructs{i}) == 1
            num = '';
        else
            num = num2str(j);
        end
        for k=1:length(fn)
            newstruct.(strcat(fn{k},num)) = cellofstructs{i}(j).(fn{k});
        end
    end
end