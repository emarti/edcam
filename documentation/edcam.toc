\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {subsection}{\numberline {1.1}Why \texttt {edcam}?}{2}
\contentsline {subsection}{\numberline {1.2}Absorption Imaging}{2}
\contentsline {subsubsection}{\numberline {1.2.1}Derivation}{3}
\contentsline {subsubsection}{\numberline {1.2.2}Noise analysis}{4}
\contentsline {subsection}{\numberline {1.3}Experimental notes}{5}
\contentsline {section}{\numberline {2}\emph {Matlab} peculiarities}{6}
\contentsline {section}{\numberline {3}Camera control}{6}
\contentsline {subsection}{\numberline {3.1}\texttt {arm} and \texttt {disarm}}{7}
\contentsline {subsection}{\numberline {3.2}\texttt {getFrames}}{7}
\contentsline {subsection}{\numberline {3.3}\texttt {getFT}}{8}
\contentsline {subsection}{\numberline {3.4}\texttt {stop}}{8}
\contentsline {section}{\numberline {4}Global variables}{9}
\contentsline {subsection}{\numberline {4.1}\texttt {temp}}{9}
\contentsline {subsection}{\numberline {4.2}\texttt {frames}}{9}
\contentsline {subsection}{\numberline {4.3}\texttt {camera}}{9}
\contentsline {subsubsection}{\numberline {4.3.1}\texttt {camera.pixelsize}}{9}
\contentsline {subsubsection}{\numberline {4.3.2}\texttt {camera.efficiency}}{9}
\contentsline {subsubsection}{\numberline {4.3.3}\texttt {camera.gain}}{9}
\contentsline {subsubsection}{\numberline {4.3.4}\texttt {camera.crosssection}}{9}
\contentsline {subsubsection}{\numberline {4.3.5}\texttt {camera.linewidth}}{9}
\contentsline {subsubsection}{\numberline {4.3.6}\texttt {camera.pulselength}}{10}
\contentsline {subsection}{\numberline {4.4}\texttt {view}}{10}
\contentsline {subsection}{\numberline {4.5}\texttt {batch}}{10}
\contentsline {subsection}{\numberline {4.6}\texttt {handles}}{10}
