%% Figure information

%This program was written by Geee Edward Marti. Use at your own risk! My email
%is emarti@berkeley.edu. Documentation is forthcoming.

%Current date: 12/03/2008

function varargout = edcam(varargin)
% EDCAM M-menu_file for edcam.fig
%      EDCAM, by itself, creates a new EDCAM or raises the existing
%      singleton*.
%
%      H = EDCAM returns the handle to a new EDCAM or the handle to
%      the existing singleton*.
%
%      EDCAM('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK zoomin EDCAM.M with the given input arguments.
%
%      EDCAM('Property','Value',...) creates a new EDCAM or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before edcam_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property
%      application
%      stop.  All inputs are passed to edcam_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text_label to modify the response to help edcam

% Last Modified by GUIDE v2.5 13-Apr-2010 15:53:32

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @edcam_OpeningFcn, ...
                   'gui_OutputFcn',  @edcam_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before edcam is made visible.
function edcam_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined zoomin a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to edcam (see VARARGIN)

% Choose default command line output for edcam
    handles.output = hObject;

global edcamroot
    edcamroot = [ matlabroot '\toolbox\edcam\current\' ];
    % edcamroot = 'C:\program files\matlab\R2008b\toolbox\edcam\current\';

global temp
    temp = struct;
    temp.z = 0;
    temp.OD = 0;
    temp.N = 0;
    temp.Nvar = 0;
    temp.fit = 0;
    
    temp.batchrunning = 0;
    temp.analyzeonopen = 0;
    temp.saveframes = 1;
    temp.acquiring = 0;
    temp.showstripchart = 0;
    temp.fullscreen = 0;
    temp.stripchart1 = [];
    temp.stripchart2 = [];
    temp.bigdisplay = 'NSum';
    
global frames
    frames = struct;
    frames.image = 0;
    frames.bright = 0;
    frames.darkimage = 0;
    frames.darkbright = 0;
   
global camera
    load([edcamroot 'currentcamera.mat']);
%     load guppy.mat;

global atom atomdata
    load([edcamroot 'atomdata.mat']);
    
global analysis
    analysis = struct;
    analysis.type = 'None';
    
    analysis.TOF = 0;

    analysis.intdata = struct; % integrated parameters -- to display and save
    analysis.fitdata = struct; %fit parameters
    analysis.chdata = struct; %crosshair parameters
    analysis.batchdata = struct;

    analysis.fitrect = [ 0 0 0 0 ];
    
    analysis.currentfitrect = 1;
    analysis.multiplefitrects = 0;
    analysis.normrect = [ 0 0 0 0 ];
    analysis.crosshair = [ 0 0 ];
    analysis.norm = 1;

    savecamera();

    analysis.fixtilt = 1;
    analysis.tilt = 0;
    
global view
    view = struct;
    view.current = 'N';
    view.rect = [ 0 0 0 0 ];

    view.ODmin = -0.2;
    view.ODmax = 0.2;
    view.imagemin = 0;
    view.imagemax = 2e4;
    view.brightmin = 0;
    view.brightmax = 2e4;
    view.darkimagemin = 0;
    view.darkimagemax = 1e4;
    view.darkbrightmin = 0;
    view.darkbrightmax = 1e4;
    view.Nmax = 5;
    view.Nmin = -5;
    
    view.zmin = view.ODmin;
    view.zmax = view.ODmax;
    view.autoscale = 1;
    view.colormap = 'camerata';
    
load palettecamerata;
global camerata
    camerata = palettecamerata;

global batch;
    batch = struct;
    batch.path = pwd;
    batch.filename = '';
    batch.num = 0;
    handles.batchall = struct;

    handles = redraw(handles);
    
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes edcam wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = edcam_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined zoomin a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


    
%% Image buttons
    
% --- Executes on button press zoomin pushbutton_generate.
function pushbutton_generate_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_generate (see GCBO)
% eventdata  reserved - to be defined zoomin a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global analysis temp frames view
%     axes(handles.axes_full);
    [x,y]=meshgrid(1:100,1:150);

    savecamera()
    if strcmp(analysis.take,'fluorescence')
        if analysis.pulselength == 0
            prefactor = (4*pi/analysis.solidangle)*2/(analysis.linewidth* ...
                100*analysis.efficiency*analysis.gain);
        else
            prefactor = (4*pi/analysis.solidangle)*2/(analysis.linewidth* ...
                analysis.pulselength*analysis.efficiency*analysis.gain);
        end
        frames.image = uint16(5 + (8*gauss2d([40,60,12,12,0,2,.2],x) + ...
            24*parabolic2d([40,60,6,12,0,2,.2],x))/prefactor);
        frames.darkimage = uint16(10);
        frames.bright = 0;
        frames.darkbright = 0;
        
        frames.image = 3 + 9*rand(size(x)) + double(analysis.gain* ...
            imnoise(frames.image,'poisson'));
        frames.darkimage = 3 + 9*rand(size(x)) + double(analysis.gain* ...
            imnoise(frames.darkimage,'poisson'));   
    else
        frames.image = uint16(3e4+0*rand(size(x))- ...
            5e2*gauss2d([40,60,12,12,0,2,.2],x) - ...
            1.5e3*parabolic2d([40,60,6,12,0,2,.2],x));
        frames.bright = uint16(3e4+0*rand(size(x)));
        
        frames.image = 4e3 + 9*rand(size(x)) + double(analysis.gain* ...
            imnoise(frames.image,'poisson'));
        frames.bright = 4e3 + 9*rand(size(x)) + double(analysis.gain* ...
            imnoise(frames.bright,'poisson'));
        frames.darkimage = 4e3 + 9*rand(size(x));
        frames.darkbright = 4e3 + 9*rand(size(x));
    end
    
    
    calc();
    
    %Is this necessary?
%     vr = view.rect;
%     xmin = vr(1); xmax = vr(2); ymin = vr(3); ymax = vr(4);
%     if (xmax == 1) || (ymax == 1)
%         xmin = 1;
%         xmax = length(temp.N(1,:));
%         ymin = 1;
%         ymax = length(temp.N(:,1));
%     end
%     view.rect = [ xmin xmax ymin ymax ];
    handles = redraw(handles);
    handles = analyze(handles);
    handles = batchstep(handles);
    guidata(hObject, handles);

% --- Executes on button press zoomin pushbutton_OD.
function pushbutton_OD_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_OD (see GCBO)
% eventdata  reserved - to be defined zoomin a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global view
    view.current = 'OD';
    handles = redraw(handles);
    handles = updatetext(handles);
    guidata(hObject, handles);

% --- Executes on button press in pushbutton_N.
function pushbutton_N_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_N (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global view
    view.current = 'N';
    handles = redraw(handles);
    handles = updatetext(handles);
    guidata(hObject, handles);


% --- Executes on button press zoomin pushbutton_image.
function pushbutton_image_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_image (see GCBO)
% eventdata  reserved - to be defined zoomin a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global view
    view.current = 'image';
    handles = redraw(handles);
    handles = updatetext(handles);
    guidata(hObject, handles);

% --- Executes on button press zoomin pushbutton_bright.
function pushbutton_bright_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_bright (see GCBO)
% eventdata  reserved - to be defined zoomin a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global view
    view.current = 'bright';
    handles = redraw(handles);
    handles = updatetext(handles);
    guidata(hObject, handles);

% --- Executes on button press zoomin pushbutton_darki.
function pushbutton_darki_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_darki (see GCBO)
% eventdata  reserved - to be defined zoomin a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global view
    view.current = 'darkimage';
    handles = redraw(handles);
    handles = updatetext(handles);
    guidata(hObject, handles);
    
% --- Executes on button press in pushbutton_darkb.
function pushbutton_darkb_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_darkb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global view
    view.current = 'darkbright';
    handles = redraw(handles);
    handles = updatetext(handles);
    guidata(hObject, handles);
    
% --- Executes on button press in pushbutton_surf.
function pushbutton_surf_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_surf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global view temp camerata
    figure
    vr = view.rect;
    x1 = vr(1); x2 = vr(2); y1 = vr(3); y2 = vr(4);
    if all(size(temp.z) >= [y2 x2]) && y2 >1 && x2 > 1
        h = surf(temp.z(y1:y2,x1:x2));
        if strcmp(view.colormap,'camerata')
            colormap(camerata);
        else
            colormap(view.colormap);
        end
        axis xy
    %     axis equal
        caxis([view.zmin view.zmax]);
    end

% --- Executes on button press in checkbox_autoscale.
function checkbox_autoscale_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_autoscale (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_autoscale
global view
    view.autoscale = get(hObject,'Value');
    handles = redraw(handles);

% --- Executes on selection change in popupmenu_colormap.
function popupmenu_colormap_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_colormap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu_colormap contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_colormap
global view
    contents = get(hObject,'String');
    view.colormap = contents{get(hObject,'Value')};
    handles = redraw(handles);
    guidata(hObject, handles);
    
% --- Executes during object creation, after setting all properties.
function popupmenu_colormap_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_colormap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in togglebutton_fullscreen.
function togglebutton_fullscreen_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton_fullscreen (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton_fullscreen
global temp
    temp.fullscreen = get(hObject,'Value');
    if temp.fullscreen
        figure(3);
        handles = redraw(handles);
    elseif ishandle(3);
        close(3);
    end

% --- Executes on button press in togglebutton_stripchart.
function togglebutton_stripchart_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton_stripchart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton_stripchart
global temp
    temp.showstripchart = get(hObject,'Value');
    if not(temp.showstripchart)
        temp.stripchart1 = [];
        temp.stripchart2 = [];
        if ishandle(2)
            close(2);
        end
    end

% --------------------------------------------------------------------
function menu_BigDisplay_Callback(hObject, eventdata, handles)
% hObject    handle to menu_BigDisplay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

function setbigdisplay(hObject,eventdata,myfield)
global temp
    temp.bigdisplay = myfield;
    handles = guidata(hObject);
    redraw(handles);
    
%     get(hObject)
%     handles = redraw(handles);

%% Zoom buttons

% --- Executes on button press zoomin zoomin.
function zoomin_Callback(hObject, eventdata, handles)
% hObject    handle to zoomin (see GCBO)
% eventdata  reserved - to be defined zoomin a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Zoom in evenly around crosshair by factors of 2
global temp analysis view
    x0 = analysis.crosshair(1); y0 = analysis.crosshair(2);
    vr = view.rect;
    xmin = vr(1); xmax = vr(2); ymin = vr(3); ymax = vr(4);

    w = round(max([ xmax - x0, x0 - xmin, ymax - y0, y0 - ymin])/2);
    xmin = max(x0 - w,1);
    xmax = min(x0 + w,length(temp.z(1,:)));
    ymin = max(y0 - w,1);
    ymax = min(y0 + w,length(temp.z(:,1)));
    view.rect = [ xmin xmax ymin ymax ];
    handles = redraw(handles);
    guidata(hObject, handles);

% --- Executes on button press in zoomout.
function zoomout_Callback(hObject, eventdata, handles)
% hObject    handle to zoomout (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global temp analysis view
    x0 = analysis.crosshair(1); y0 = analysis.crosshair(2);
    vr = view.rect;
    xmin = vr(1); xmax = vr(2); ymin = vr(3); ymax = vr(4);
    
    w = round(max([ xmax - x0, x0 - xmin, ymax - y0, y0 - ymin])*2);
    xmin = max(x0 - w,1);
    xmax = min(x0 + w,length(temp.z(1,:)));
    ymin = max(y0 - w,1);
    ymax = min(y0 + w,length(temp.z(:,1)));
    view.rect = [ xmin xmax ymin ymax ];
    handles = redraw(handles);
    guidata(hObject, handles);

% --- Executes on button press in zoomfull.
function zoomfull_Callback(hObject, eventdata, handles)
% hObject    handle to zoomfull (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global temp view
    view.rect = [ 1 length(temp.z(1,:)) 1 length(temp.z(:,1)) ];
    handles = redraw(handles);
    guidata(hObject, handles);

%% Scale buttons

% --- Executes on button press zoomin pushbutton_zmax_plus.
function pushbutton_zmax_plus_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_zmax_plus (see GCBO)
% eventdata  reserved - to be defined zoomin a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global view
    temp = view.zmax*2^(-1/3);
    if temp > view.zmin
        view.zmax = temp;
        savezoom();
        handles = redraw(handles);
    end
    guidata(hObject, handles);

% --- Executes on button press zoomin pushbutton_zmax_minus.
function pushbutton_zmax_minus_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_zmax_minus (see GCBO)
% eventdata  reserved - to be defined zoomin a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global view
    view.zmax = view.zmax*2^(1/3);
    savezoom();
    handles = redraw(handles);
    guidata(hObject, handles);

function edit_zmax_Callback(hObject, eventdata, handles)
% hObject    handle to edit_zmax (see GCBO)
% eventdata  reserved - to be defined zoomin a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_zmax as text_label
%        str2double(get(hObject,'String')) returns contents of edit_zmax as a double
global view
    [zmax, stat] = str2num(get(handles.edit_zmax,'String'));
    if stat
        if zmax > view.zmin
            view.zmax = zmax;
        end
    end
    savezoom();
    handles = redraw(handles);
    guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edit_zmax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_zmax (see GCBO)
% eventdata  reserved - to be defined zoomin a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in zminplus.
function pushbutton_zmin_plus_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_zmin_plus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global view
    view.zmin = view.zmin*2^(-1/3);
    savezoom();
    handles = redraw(handles);
    guidata(hObject, handles);

% --- Executes on button press in pushbutton_zmin_minus.
function pushbutton_zmin_minus_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_zmin_minus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global view
    temp = view.zmin*2^(1/3);
    if temp < view.zmax
        view.zmin = temp;
        savezoom();
        handles = redraw(handles);
    end
    guidata(hObject, handles);


function edit_zmin_Callback(hObject, eventdata, handles)
% hObject    handle to edit_zmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_zmin as text
%        str2double(get(hObject,'String')) returns contents of edit_zmin
%        as a double
global view
    [zmin, stat] = str2num(get(handles.edit_zmin,'String'));
    if stat
        if zmin < view.zmax
            view.zmin = zmin;
        end
    end
    savezoom();
    handles = redraw(handles);
    guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edit_zmin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_zmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Camera buttons

% --- Executes on button press in pushbutton_arm.
function pushbutton_arm_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_arm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global camera
    if arm()
        camera.iscameraready = 1;
    else
        camera.iscameraready = 0;
    end
    guidata(hObject, handles);

% --- Executes on button press in pushbutton_stop.
function pushbutton_stop_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_stop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    stop();
    guidata(hObject, handles);

% --- Executes on button press in pushbutton_disarm.
function pushbutton_disarm_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_disarm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global camera
    disarm();
    camera.iscameraready = 0;
    guidata(hObject, handles);
    
    
% --- Executes on button press in pushbutton_start.
function pushbutton_start_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global temp frames camera
if (temp.acquiring)
    set(hObject,'Value',1);
    guidata(hObject, handles);
else
    set(hObject,'Value',1);
    guidata(hObject, handles);
    handles = acquire(handles);
    set(hObject,'Value',0)
    guidata(hObject, handles);
end

% --------------------------------------------------------------------
function uipanel_picturechoice_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to uipanel_picturechoice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global temp camera
    camera.take = get(hObject,'String');
    if temp.acquiring
        stop();
        handles = acquire(handles);
    end
    guidata(hObject,handles);
    
   
% --- Executes on button press in checkbox_externaltrig.
function checkbox_externaltrig_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_externaltrig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_externaltrig
global camera
    camera.externaltrigger = get(hObject,'Value');
    
% --- Executes on button press in checkbox_continuous.
function checkbox_continuous_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_continuous (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_continuous
global camera
    camera.continuous = get(hObject,'Value');

% --- Executes on button press in pushbutton_setROI.
function pushbutton_setROI_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_setROI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global camera view
    camera.roirect = view.rect;
    guidata(hObject, handles);

% --- Executes on button press in pushbutton_fullROI.
function pushbutton_fullROI_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_fullROI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global camera
    camera.roirect = [ 1 camera.CCD(2) 1 camera.CCD(1) ];
    guidata(hObject, handles);

function edit_exposuretime_Callback(hObject, eventdata, handles)
% hObject    handle to edit_exposuretime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_exposuretime as text
%        str2double(get(hObject,'String')) returns contents of edit_exposuretime as a double
global camera
    exposuretime = str2double(get(hObject,'String'));
    if (isempty(get(hObject,'String'))) || isnan(exposuretime)
        set(hObject,'String',camera.exposuretime);
    else
        camera.exposuretime = exposuretime;
    end
    guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function edit_exposuretime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_exposuretime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function popupmenu_camera_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_camera (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupmenu_camera.
function popupmenu_camera_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_camera (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu_camera contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_camera
global camera edcamroot
    contents = get(hObject,'String');
    camera.type = contents{get(hObject,'Value')};
switch camera.type
    case 'Andor iKon'
        load([edcamroot 'ikon.mat']);
    case 'Andor iXon'
        load([edcamroot 'ixon.mat']);
    case 'Guppy'
        load([edcamroot 'guppy.mat']);
    case 'Stingray'
        load([edcamroot 'stingray.mat']);
end
    handles = redraw(handles);
    guidata(hObject, handles);

%% Clear buttons

% --- Executes on button press in pushbutton_.
function pushbutton_clearnorm_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_clearnorm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global analysis
    analysis.normrect = [1 1 1 1];
    
    calc();
    handles = redraw(handles);
    handles = analyze(handles);
    guidata(hObject, handles);

% --- Executes on button press in pushbutton_clearcrosshairs.
function pushbutton_clearcrosshairs_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_clearcrosshairs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global analysis
    analysis.crosshair = [ 0 0 ];
    handles = redraw(handles);
    handles = updatetext(handles);
    guidata(hObject, handles);

% --- Executes on button press in pushbutton_clearfit.
function pushbutton_clearfit_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_clearfit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global temp analysis
    analysis.fitrect(analysis.currentfitrect,:) = [ 0 0 0 0 ];
    handles = analyze(handles);
    guidata(hObject, handles);

% --- Executes on button press in pushbutton_clearallfits.
function pushbutton_clearallfits_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_clearallfits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global temp analysis
    analysis.fitrect = [ 0 0 0 0 ];
    handles = analyze(handles);
    guidata(hObject, handles);
    
% --- Executes on button press in pushbutton_clearframes.
function pushbutton_clearframes_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_clearframes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of pushbutton_clearframes
global temp frames
    temp.z = 0;
    temp.OD = 0;
    temp.N = 0;
    temp.Nvar = 0;
    temp.fit = 0;
    
    frames.image = 0;
    frames.bright = 0;
    frames.darkimage = 0;
    frames.darkbright = 0;

    handles = redraw(handles);

%% Analysis buttons

% --- Executes on selection change in popupmenu_analysis.
function popupmenu_analysis_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_analysis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu_analysis contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_analysis
global analysis
    contents = get(hObject,'String');
    analysis.type = contents{get(hObject,'Value')};
    handles = analyze(handles);
    guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_analysis_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_analysis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_TOF_Callback(hObject, eventdata, handles)
% hObject    handle to edit_TOF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_TOF as text
%        str2double(get(hObject,'String')) returns contents of edit_TOF as a double
global analysis
    TOF = str2double(get(hObject,'String'));
    if (isempty(get(hObject,'String')))
        set(hObject,'String',0);
        analysis.TOF = 0;
    elseif isnan(TOF)
        set(hObject,'String',analysis.TOF);
    else
        analysis.TOF = TOF;
    end
    analyze(handles);

% --- Executes during object creation, after setting all properties.
function edit_TOF_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_TOF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton_noise.
function pushbutton_noise_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_noise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global temp frames analysis
    fr = analysis.fitrect(analysis.currentfitrect,:);
    x1 = fr(1); x2 = fr(2); y1 = fr(3); y2 = fr(4);
    if (x2 > x1) && (y2 > y1) && all(size(temp.N) >= [y2 x2])
        if isequal(frames.darkimage,0)
            darkimage = zeros(size(temp.N));
        else
            darkimage = frames.darkimage;
        end
        
        if isequal(frames.darkbright,0)
            darkbright = frames.darkimage;
        else
            darkbright = frames.darkbright;
        end
        
% %         image = reshape(frames.image(y1:y2,x1:x2) - darkimage(y1:y2,x1:x2),(y2-y1+1)*(x2-x1+1),1);
% %         bright = reshape(frames.bright(y1:y2,x1:x2) - darkbright(y1:y2,x1:x2),(y2-y1+1)*(x2-x1+1),1);
%         image = reshape(frames.image(y1:y2,x1:x2),(y2-y1+1)*(x2-x1+1),1);
%         bright = reshape(frames.bright(y1:y2,x1:x2),(y2-y1+1)*(x2-x1+1),1);
% %         sk = (bright-image)./sqrt(analysis.gain*(bright+image)); % normalized Skellam distribution
%         sk = (bright-image);
%         skdx = 1; %std(sk)/10;
%         range = 50; %5*std(sk);
%         skx = transpose(-range:skdx:range);    
%         skhist = histc(sk,skx);
%         skgauss = length(sk)*skdx./(std(sk)*sqrt(2*pi))*exp(-((skx-mean(sk)).^2)/(2*std(sk)^2));
%         figure
%         subplot(1,1,1);
%         plot(skx,skhist,skx,skgauss);
%         axis tight;
%         text(min(skx)*0.9,max(skhist)*.7,sprintf('mean = %0.4G\n std dev/sqrt(2) = %0.4G',[mean(sk) std(sk)/sqrt(2)]),'FontSize',12)
        
%         ODlist = reshape(temp.N(y1:y2,x1:x2),(y2-y1+1)*(x2-x1+1),1);
%         expected = sqrt(mean(1./bright+1./image)/analysis.gain);
%         ODdx = std(ODlist)/10;
%         ODrange = 5*std(ODlist);
%         ODx = transpose(-ODrange:ODdx:ODrange);
%         ODhist = histc(ODlist,ODx);
%         ODgauss = length(ODlist)*ODdx./(std(ODlist)*sqrt(2*pi))*exp(-((ODx-mean(ODlist)).^2)/(2*std(ODlist)^2));
%         subplot(3,1,2);
%         plot(ODx,ODhist,ODx,ODgauss);
%         axis tight;
%         text(min(ODx)*0.9,max(ODhist)*.7,sprintf('OD\nmean = %0.4G\nstd dev = %0.4G\nexpected = %0.4G',[mean(ODlist) std(ODlist) expected]),'FontSize',12)
        
        figure


        Nlist = reshape(temp.N(y1:y2,x1:x2),(y2-y1+1)*(x2-x1+1),1);
        Nvarlist = reshape(temp.Nvar(y1:y2,x1:x2),(y2-y1+1)*(x2-x1+1),1);
        Nexpected = sqrt(mean(Nvarlist));
        Ndx = std(Nlist)/10;
        Nrange = 5*std(Nlist);
        Nx = transpose(-Nrange:Ndx:Nrange);
        Nhist = histc(Nlist,Nx);
%         Ngauss =
%         length(Nlist)*Ndx./(std(Nlist)*sqrt(2*pi))*exp(-((Nx-mean(Nlist)).^2)/(2*std(Nlist)^2));
        Ngauss = length(Nlist)*Ndx./(Nexpected*sqrt(2*pi))* ...
            exp(-((Nx-mean(Nlist)).^2)/(2*Nexpected^2));
        subplot(1,1,1);
        plot(Nx,Nhist,Nx,Ngauss);
        axis tight;
        text(min(Nx)*0.9,max(Nhist)*.7,sprintf('N\nmean = %0.4G\nstd dev = %0.4G\nexpected = %0.4G',[mean(Nlist) std(Nlist) Nexpected]),'FontSize',12)
        
    end

    
% --- Executes on button press in checkbox_analyze.
function checkbox_analyze_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_analyze (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_analyze
global temp
    temp.analyzeonopen = get(hObject,'Value');
    guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function text_label_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined zoomin a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function text_data_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_data (see GCBO)
% eventdata  reserved - to be defined zoomin a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit_pulselength_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_pulselength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_pulselength_Callback(hObject, eventdata, handles)
% hObject    handle to edit_pulselength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_pulselength as text
%        str2double(get(hObject,'String')) returns contents of edit_pulselength as a double
global camera
    pulselength = str2double(get(hObject,'String'));
    if (isempty(get(hObject,'String')))
        set(hObject,'String',0);
        camera.pulselength = 0;
    elseif isnan(pulselength)
        set(hObject,'String',camera.pulselength);
    else
        camera.pulselength = pulselength;
    end
    guidata(hObject, handles);
    
    % --- Executes on button press in checkbox_fixtilt.
function checkbox_fixtilt_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_fixtilt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_fixtilt
global analysis
    analysis.fixtilt = get(hObject,'Value');
    handles = analyze(handles);
    handles = redraw(handles);

function edit_tilt_Callback(hObject, eventdata, handles)
% hObject    handle to edit_tilt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_tilt as text
%        str2double(get(hObject,'String')) returns contents of edit_tilt as a double
global analysis
    tilt = str2double(get(hObject,'String'));
    if (isempty(get(hObject,'String')))
        set(hObject,'String',0);
        analysis.tilt = 0;
    elseif isnan(tilt)
        set(hObject,'String',analysis.tilt);
    else
        analysis.tilt = tilt*pi/180;
    end
    analyze(handles);

% --- Executes during object creation, after setting all properties.
function edit_tilt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_tilt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in checkbox_multiplefitrects.
function checkbox_multiplefitrects_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_multiplefitrects (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_multiplefitrects
global analysis
    analysis.multiplefitrects = get(hObject,'Value');
    handles = redraw(handles);
    if not(analysis.multiplefitrects)
        handles = analyze(handles);
    end
	guidata(hObject,handles);

% --------------------------------------------------------------------
function uipanel_fitrect_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to uipanel_fitrect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global analysis
    currentfitrect = str2double(get(hObject,'String'));
    analysis.currentfitrect = currentfitrect;
    handles = analyze(handles);
	guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit_crosssection_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_zmax (see GCBO)
% eventdata  reserved - to be defined zoomin a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_crosssection_Callback(hObject, eventdata, handles)
% hObject    handle to edit_crosssection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_crosssection as text
%        str2double(get(hObject,'String')) returns contents of edit_crosssection as a double
global atom
    crosssection = str2double(get(hObject,'String'));
    if (isempty(get(hObject,'String'))) || isnan(crosssection)
        set(hObject,'String',atom.crosssection);
    else
        atom.crosssection = crosssection;
    end
    guidata(hObject, handles);


%% Batch buttons
% --- Executes on button press in pushbutton_startbatch.
function pushbutton_startbatch_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_startbatch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global temp batch
    if (temp.batchrunning)
        set(hObject,'Value',1);
        guidata(hObject, handles);
    else
        [filename, pathname, filterindex] = uiputfile({'*.mat','Mat-Files'},'Save Batch');
        if not(filterindex == 0)
            if length(filename) > 3 % the filename should not have a '.mat' at the end
                                    % this can be done more cleanly with 'fileparts'
                if strcmpi(filename((length(filename)-3):length(filename)),'.mat')
                    filename = filename(1:(length(filename)-4));
                end
            end
            cd(pathname);
            batch.path = pathname;
            batch.filename = filename;
            batch.num = 0;
            temp.batchrunning = 1;
            handles.batchall = struct;
            set(hObject,'Value',1);
            guidata(hObject, handles);
        else
            set(hObject,'Value',0);
            guidata(hObject, handles);
        end
    end

% --- Executes on button press in pushbutton_stopbatch.
function pushbutton_stopbatch_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_stopbatch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global temp analysis batch
    temp.batchrunning = 0;
    set(handles.pushbutton_startbatch,'Value',0);
    handles.batchall = struct;
    handles = updatetext(handles);
    guidata(hObject, handles);

% --- Executes on button press in pushbutton_movie.
function pushbutton_movie_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_movie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global temp frames analysis view batch camerata
%     [filename, pathname, filterindex] = uiputfile({'*.mat','Mat-Files'},'Movie File');
%     if not(filterindex == 0)
%         if length(filename) > 3 % the filename should not have a '.mat' at the end
%                                 % this can be done more cleanly with 'fileparts'
%             if strcmpi(filename((length(filename)-3):length(filename)),'.mat')
%                 filename = filename(1:(length(filename)-4));
%             end
%         end
%         cd(pathname);
%         batch.path = pathname;
%         batch.filename = filename;
%         batch.num = 0;
%         temp.batchrunning = 1;
%         handles.batchall = struct;
%         set(hObject,'Value',1);
%         guidata(hObject, handles);
        
        clear MovieMat;

        [getfilenames, pathname, filterindex] = uigetfile({'*.mat','Matlab File'}, ...
            'Select Files','MultiSelect','on');
       
        if not(filterindex == 0)
            i = 1;
            while (i <= length(getfilenames))
                if (temp.analyzeonopen)
                    load([ pathname getfilenames{i} ], 'frames');
                    savecamera();
                else
                    load([ pathname getfilenames{i} ], 'frames','analysis','view');
                end
                analysis.batchdata.filename = getfilenames{i};
                
                calc();
                handles = redraw(handles);
                handles = analyze(handles);
                handles = updatetext(handles);
                guidata(hObject, handles);
                
                %make figure
                figure(3);clf;
                vr = view.rect;
                x1 = vr(1); x2 = vr(2); y1 = vr(3); y2 = vr(4);
                imagesc(temp.z(y1:y2,x1:x2));
                if strcmp(view.colormap,'camerata')
                    colormap(camerata);
                else
                    colormap(view.colormap);
                end
                axis xy
                axis equal
                caxis([view.zmin view.zmax]);
                MovieMat(i) = getframe(gca);
%                 pause(1);
                i = i+1;
            end
        end
    set(hObject,'Value',0);
    handles.batchall = struct;
    handles = updatetext(handles);
    guidata(hObject, handles);
    movie2avi(MovieMat,'movie.avi','fps',4)
    

% --- Executes on button press in pushbutton_makebatch.
function pushbutton_makebatch_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_makebatch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global temp frames analysis view batch
    [filename, pathname, filterindex] = uiputfile({'*.mat','Mat-Files'},'New Batch File');
    if not(filterindex == 0)
        if length(filename) > 3 % the filename should not have a '.mat' at the end
                                % this can be done more cleanly with 'fileparts'
            if strcmpi(filename((length(filename)-3):length(filename)),'.mat')
                filename = filename(1:(length(filename)-4));
            end
        end
        cd(pathname);
        batch.path = pathname;
        batch.filename = filename;
        batch.num = 0;
        temp.batchrunning = 1;
        handles.batchall = struct;
        set(hObject,'Value',1);
        guidata(hObject, handles);
        
        [getfilenames, pathname, filterindex] = uigetfile({'*.mat','Matlab File'}, ...
            'Select Files','MultiSelect','on');
        if not(filterindex == 0)
            i = 1;
            while (i <= length(getfilenames)) && temp.batchrunning
                if (temp.analyzeonopen)
                    load([ pathname getfilenames{i} ], 'frames');
                    savecamera();
                else
                    load([ pathname getfilenames{i} ], 'frames','analysis','view');
                end
                analysis.batchdata.filename = getfilenames{i};
                
                calc();
                handles = redraw(handles);
                handles = analyze(handles);
                handles = updatetext(handles);
                handles = batchstep(handles);
                guidata(hObject, handles);
                i = i+1;
            end
        end
    end
    temp.batchrunning = 0;
    set(hObject,'Value',0);
    handles.batchall = struct;
    handles = updatetext(handles);
    guidata(hObject, handles);



% --- Executes on button press in pushbutton_batchprev.
function pushbutton_batchprev_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_batchprev (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton_batchnext.
function pushbutton_batchnext_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_batchnext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in checkbox_autosaveframes.
function checkbox_autosaveframes_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_autosaveframes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_autosaveframes
global temp
    temp.saveframes = get(hObject,'Value');
    guidata(hObject, handles);

%% Keys and Mouse Clicks

% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined zoomin a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global analysis view
% set(handles.figure1,'currentAxes',handles.axes_full);
switch get(gcf,'SelectionType')
    case 'extend'
        point1 = get(handles.axes_full,'CurrentPoint');
        px1 = point1(1,1); py1 = point1(1,2);
        vr = view.rect;
        xmin = vr(1); xmax = vr(2); ymin = vr(3); ymax = vr(4);
        if (xmin <= px1) && (px1 <= xmax) && (ymin <= py1) && (py1 <= ymax) && (strcmp(view.current,'OD') || strcmp(view.current,'N'))
            rbbox;
            point2 = get(handles.axes_full,'CurrentPoint');

            px2 = point2(1,1); py2 = point2(1,2);
            x1=round(max(min(px1,px2),xmin));
            y1=round(max(min(py1,py2),ymin));
            x2=round(min(max(px1,px2),xmax));
            y2=round(min(max(py1,py2),ymax));
            
            if (x2 > x1 + 1) && (y2 > y1 + 1)
                analysis.fitrect(analysis.currentfitrect,:) = [ x1 x2 y1 y2 ];

                handles = redraw(handles);
                handles = analyze(handles);
                guidata(hObject, handles);
            end
        end
     case 'alt'
        point1 = get(handles.axes_full,'CurrentPoint');
        px1 = point1(1,1); py1 = point1(1,2);
        vr = view.rect;
        xmin = vr(1); xmax = vr(2); ymin = vr(3); ymax = vr(4);
        if (xmin <= px1) && (px1 <= xmax) && (ymin <= py1) && (py1 <= ymax)
            rbbox;
            point2 = get(handles.axes_full,'CurrentPoint');
            px2 = point2(1,1); py2 = point2(1,2);
            x1=ceil(max(min(px1,px2),xmin));
            y1=ceil(max(min(py1,py2),ymin));
            x2=floor(min(max(px1,px2),xmax));
            y2=floor(min(max(py1,py2),ymax));
            
            if (x2 > x1 + 1) && (y2 > y1 + 1)
                view.rect = [ x1 x2 y1 y2 ];
                handles = redraw(handles);
                guidata(hObject, handles);
            end
        end
    case 'open'
        point = get(handles.axes_full,'CurrentPoint');
        x = round(point(1,1)); y = round(point(1,2));
        vr = view.rect;
        xmin = vr(1); xmax = vr(2); ymin = vr(3); ymax = vr(4);

        if (xmin <= x) && (x <= xmax) && (ymin <= y) && (y <= ymax)
            analysis.crosshair = [ x y ];

            handles = redraw(handles);

            handles = updatetext(handles);
            guidata(hObject, handles);
        end
    case 'normal'
        switch get(gcf,'CurrentCharacter')
            case {'r', 'R'}
                point1 = get(handles.axes_full,'CurrentPoint');
                px1 = point1(1,1); py1 = point1(1,2);
                vr = view.rect;
                xmin = vr(1); xmax = vr(2); ymin = vr(3); ymax = vr(4);
                if (xmin <= px1) && (px1 <= xmax) && (ymin <= py1) && (py1 <= ymax)
                    rbbox;
                    point2 = get(handles.axes_full,'CurrentPoint');

                    px2 = point2(1,1); py2 = point2(1,2);
                    x1=ceil(max(min(px1,px2),xmin));
                    y1=ceil(max(min(py1,py2),ymin));
                    x2=floor(min(max(px1,px2),xmax));
                    y2=floor(min(max(py1,py2),ymax));

                    if (x2 > x1 + 1) && (y2 > y1 + 1)
                        analysis.normrect = [ x1 x2 y1 y2 ];
                        
                        calc();
                        handles = redraw(handles);
                        handles = analyze(handles);
                        guidata(hObject, handles);
                    end
                end
            case {'d', 'D'}
               point1 = get(handles.axes_full,'CurrentPoint');
                x1 = round(point1(1,1)); y1 = round(point1(1,2));
                vr = view.rect;
                xmin = vr(1); xmax = vr(2); ymin = vr(3); ymax = vr(4);
                if (xmin <= x1) && (x1 <= xmax) && (ymin <= y1) && (y1 <= ymax)
                    rbbox;
                    point2 = get(handles.axes_full,'CurrentPoint');

                    px2 = point2(1,1); py2 = point2(1,2);

                    x2 = round(max(min(px2,xmax),xmin));
                    y2 = round(max(min(py2,ymax),ymin));
                    
                    if (x1 ~= x2) || (y1 ~= y2)
                        handles = redraw(handles);
%                         axes(handles.axes_full);
                        set(0,'CurrentFigure',handles.figure1);
                        set(handles.figure1,'currentAxes',handles.axes_full);
                        line([x1 x2],[y1 y2],'Color','g');
                        line([x1 x1],[y1 y2],'Color','g');
                        line([x1 x2],[y2 y2],'Color','g');
                        distx = sprintf('%0.1f',analysis.pixelsize*abs(x2-x1));
                        disty = sprintf('%0.1f',analysis.pixelsize*abs(y2-y1));
                        dist = sprintf('%0.1f',analysis.pixelsize*sqrt((x2-x1)^2+(y2-y1)^2));
                        
                        xavg = (x1+x2)/2;
                        yavg = (y1+y2)/2;
                        
                        dx = (xmax - xmin)/10;
                        dy = (ymax - ymin)/20;
                        if x1 > x2
                            dx = -dx;
                        end

                        if y1 > y2
                            dy = -dy;
                        end

                        text(xavg+dx,yavg-dy,dist,'Color','g','FontSize',14,'HorizontalAlignment','center');
                        if (x1 ~= x2) && (y1 ~= y2)
                            text(xavg,y2+dy,distx,'Color','g','FontSize',14,'HorizontalAlignment','center');
                            text(x1-dx,yavg,disty,'Color','g','FontSize',14,'HorizontalAlignment','center');
                        end
                    end
                end 
        end
end


% --- Executes on key press over figure1 with no controls selected.
function figure1_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined zoomin a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global analysis view
    x = analysis.crosshair(1); y = analysis.crosshair(2);
    switch get(gcf,'CurrentCharacter')
        case '+' % '='
            pushbutton_zmax_plus_Callback(hObject, eventdata, handles);
        case '-'
            pushbutton_zmax_minus_Callback(hObject, eventdata, handles);
%         case '+'
%             pushbutton_zmin_plus_Callback(hObject, eventdata, handles);
%         case '_'
%             pushbutton_zmin_minus_Callback(hObject, eventdata, handles);
        case 'i'
            zoomin_Callback(hObject, eventdata, handles);
        case 'o'
            zoomout_Callback(hObject, eventdata, handles);
        case 'f'
            zoomfull_Callback(hObject, eventdata, handles);
        case '0'
            pushbutton_N_Callback(hObject, eventdata, handles);
        case '1'
            pushbutton_OD_Callback(hObject, eventdata, handles);
        case '2'
            pushbutton_image_Callback(hObject, eventdata, handles);
        case '3'
            pushbutton_bright_Callback(hObject, eventdata, handles);
        case '4'
            pushbutton_darki_Callback(hObject, eventdata, handles);
        case '5'
            pushbutton_darkb_Callback(hObject, eventdata, handles);
        case 28 % left key
            x = x - 1;
        case 29 % right key
            x = x + 1;
        case 30
            y = y + 1;
        case 31
            y = y - 1;
        case 'n'
            choices = get(handles.uipanel_fitrect,'Children');
            if analysis.multiplefitrects
                if max(str2double(get(choices,'String'))) > analysis.currentfitrect
                  analysis.currentfitrect = analysis.currentfitrect + 1;
                else
                    analysis.currentfitrect = 1;
                end
                handles = analyze(handles);
            end
    end
    vr = view.rect;
    xmin = vr(1); xmax = vr(2); ymin = vr(3); ymax = vr(4);
    x0 = analysis.crosshair(1); y0 = analysis.crosshair(2);

    if not(x == x0 && y == y0) && (xmin <= x) && (x <= xmax) && (ymin <= y) && (y <= ymax)
        analysis.crosshair = [ x y ];
        handles = redraw(handles);
        guidata(hObject, handles);
    end
 
%% Menu    

% --------------------------------------------------------------------
function menu_file_Callback(hObject, eventdata, handles)
% hObject    handle to menu_file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function menu_help_Callback(hObject, eventdata, handles)
% hObject    handle to menu_help (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% % --------------------------------------------------------------------
% function menu_exposuresettings_Callback(hObject, eventdata, handles)
% % hObject    handle to menu_exposuresettings (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % --------------------------------------------------------------------
% function menu_camera_Callback(hObject, eventdata, handles)
% % hObject    handle to menu_camera (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function menu_save_Callback(hObject, eventdata, handles)
% hObject    handle to menu_save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global frames analysis view camera
    [filename, pathname, filterindex] = uiputfile({'*.mat','Mat-Files'},'Save');
    if not(filterindex == 0)
%     [pathstr, name, ext] = fileparts(filename)
        if length(filename) > 3 % the filename should not have a '.mat' at the end
                                 % this can be done more cleanly with 'fileparts'
            if strcmpi(filename((length(filename)-3):length(filename)),'.mat')
                filename = filename(1:(length(filename)-4));
            end
        end
        save([ pathname filename '.mat' ],'frames','analysis','view','camera');
        cd(pathname);
    end

% --------------------------------------------------------------------
function menu_makefig_Callback(hObject, eventdata, handles)
% hObject    handle to menu_makefig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global temp view camerata
    figure
    vr = view.rect;
    x1 = vr(1); x2 = vr(2); y1 = vr(3); y2 = vr(4);
    h = imagesc(temp.z(y1:y2,x1:x2));
    
    if strcmp(view.colormap,'camerata')
        colormap(camerata);
    else
        colormap(view.colormap);
    end
    axis xy
    axis equal
    caxis([view.zmin view.zmax]);

% --------------------------------------------------------------------
function menu_savecurrentview_Callback(hObject, eventdata, handles)
% hObject    handle to menu_savecurrentview (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global temp view camerata
    vr = view.rect;
    xmin = vr(1); xmax = vr(2); ymin = vr(3); ymax = vr(4);

    myimage = temp.z(ymin:ymax,xmin:xmax);
    myimage = flipud(uint8(255*(myimage-view.zmin)/(view.zmax-view.zmin)));
%     myimage = flipud(uint16(65535*(myimage-view.zmin)/(view.zmax-view.zmin)));
    if strcmp(view.colormap,'camerata')
        myimage2 = ind2rgb(myimage,camerata);
    else
        myimage2 = ind2rgb(myimage,colormap(view.colormap));
    end
    
    [filename, pathname, filterindex] = uiputfile({'*.tif','TIFF (this is the best mode) (*.tif)';
        '*.gif','GIF (*.gif)';'*.bmp','BMP (*.bmp)';'*.jpg','JPEG (*.jpg)'},'Save Current View');
%     imwrite(myimage,[ pathname filename ]);
    switch filterindex
        case 1
            imwrite(myimage2,[ pathname filename ],'tif',...
                'Compression','none','Resolution',[size(myimage,1) size(myimage,2)]);
        case 2
            imwrite(myimage,[ pathname filename ],'gif');
        case 3
            imwrite(myimage2,[ pathname filename ],'bmp');
        case 4
            imwrite(myimage2,[ pathname filename ],'jpg','Bitdepth',8,'Mode','lossy','Quality',100);
    end
    if filterindex
        cd(pathname);
    end

% --------------------------------------------------------------------
function menu_open_Callback(hObject, eventdata, handles)
% hObject    handle to menu_open (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global temp frames analysis view batch
[filename, pathname, filterindex] = uigetfile({'*.mat','Matlab File'},'Open');
    if not(filterindex == 0)
        if (temp.analyzeonopen)
            load([ pathname filename ], 'frames');
            savecamera();
        else
            load([ pathname filename ], 'frames','analysis','view','batch');
        end
        cd(pathname);
        
        if not(isfield(analysis,'fixtilt'))
            analysis.fixtilt = get(handles.checkbox_fixtilt,'Value');
        end
        if not(isfield(analysis,'tilt'))
            analysis.tilt = get(handles.edit_tilt,'Value')*pi/180;
        end

        if not(isfield(analysis,'readoutnoise'))
            analysis.readoutnoise = 10;
        end
        
        if not(isfield(analysis,'multiplefitrects'))
            analysis.multiplefitrects = 0;
        end
        
        if not(isfield(view,'autoscale'))
            view.autoscale = 0;
        end
        
        if not(isfield(view,'colormap'))
            view.colormap = 'camerata';
        end

        if not(isfield(analysis,'take'))
            analysis.take = 'OD with dark';
        end
        
        if not(isfield(analysis,'norm'))
            analysis.norm = 1;
        end
        
        if not(isfield(analysis,'scatteringrate'))
            analysis.scatteringrate = analysis.linewidth/2;
        end
        
        calc();
        handles = redraw(handles);
        handles = analyze(handles);
        guidata(hObject, handles);
    end




    
% --------------------------------------------------------------------
function menu_about_Callback(hObject, eventdata, handles)
% hObject    handle to menu_about (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
cr = sprintf('\n');
msgbox([ 'Created by Ed (emarti@berkeley.edu)' cr '12/03/2008' ],'About');


% --------------------------------------------------------------------
function menu_keyboardshortcuts_Callback(hObject, eventdata, handles)
% hObject    handle to menu_keyboardshortcuts (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
cr = sprintf('\n');
text = [ 'Ctrl + mouse drag: zoom rectangle' cr ...
    'Shift + mouse drag: fit rectange (N and OD views only)' cr ...
    'r + mouse drag: normalization rectange' cr ...
    'n: next fit rectange' cr ...
    'i: zoom in' cr 'o: zoom out' cr ...
    'f: full view' cr '+: increase scale' cr '-: decrease scale' cr ...
    '0: Atomic Density (N)' cr '1: Optical Density (OD)' cr ...
     '2: Absorption image' cr '3: Bright image' cr ...
     '4: Dark image (absorption)' cr '5: Dark image (bright)' cr ...
     'Arrow keys: moves crosshair' ];

msgbox(text,'Keyboard Shortcuts');

    
%% Camera functions
function pass = arm()
global camera
switch camera.type
    case 'Andor iKon-M'
        pass = armAndor();
    case 'Andor iXon'
        pass = armIxon();
    case 'Guppy'
        pass = armGuppy();
    case 'Stingray'
        pass = armStingray();
end

function disarm()
global camera
switch camera.type
    case 'Andor iKon-M'
        disarmAndor();
    case 'Andor iXon'
        disarmIxon();
    case 'Guppy'
        disarmGuppy();
    case 'Stingray'
        disarmStingray();
end

function stop()
global camera temp
    temp.acquiring = 0;
switch camera.type
    case 'Andor iKon-M'
        stopAndor();
    case 'Andor iXon'
        stopIxon();
    case 'Guppy'
        stopGuppy();
    case 'Stingray'
        stopStingray();
end

function myimages = getFrames(nOfFrames, exposureTime, useTrigger, useShutter, ROIStartX, ROIStopX, ROIStartY, ROIStopY)
global camera
switch camera.type
    case 'Andor iKon-M'
        myimages = getFramesAndor(nOfFrames, exposureTime, useTrigger, useShutter, ROIStartX, ROIStopX, ROIStartY, ROIStopY);
    case 'Andor iXon'
        myimages = getFramesIxon(nOfFrames, exposureTime, useTrigger, useShutter, ROIStartX, ROIStopX, ROIStartY, ROIStopY);
    case 'Guppy'
        myimages = getFramesGuppy(nOfFrames, exposureTime, useTrigger, useShutter, ROIStartX, ROIStopX, ROIStartY, ROIStopY);
    case 'Stingray'
        myimages = getFramesStingray(nOfFrames, exposureTime, useTrigger, useShutter, ROIStartX, ROIStopX, ROIStartY, ROIStopY);
end

function myimages = getFT(nOfFrames, framesPerCCD, exposureTime, useTrigger, useShutter, ROIStartX, ROIStopX, ROIStartY, ROIStopY)
global camera
switch camera.type
    case 'Andor iKon-M'
        myimages = getFTAndor(nOfFrames, framesPerCCD, exposureTime, useTrigger, useShutter, ROIStartX, ROIStopX, ROIStartY, ROIStopY);
    case 'Andor iXon'
        myimages = getFTIxon(nOfFrames, framesPerCCD, exposureTime, useTrigger, useShutter, ROIStartX, ROIStopX, ROIStartY, ROIStopY);
    case 'Guppy'
        myimages = getFTGuppy(nOfFrames, framesPerCCD, exposureTime, useTrigger, useShutter, ROIStartX, ROIStopX, ROIStartY, ROIStopY);
    case 'Stingray'
        myimages = getFTGuppy(nOfFrames, framesPerCCD, exposureTime, useTrigger, useShutter, ROIStartX, ROIStopX, ROIStartY, ROIStopY);        
end

function myimages = getVideo(exposureTime, ROIStartX, ROIStopX, ROIStartY, ROIStopY)
global camera
switch camera.type
    case 'Andor iKon-M'
        myimages = getFramesAndor(1, exposureTime, 0, 1, ROIStartX, ROIStopX, ROIStartY, ROIStopY);
    case 'Andor iXon'
        myimages = getVideoIxon(exposureTime, ROIStartX, ROIStopX, ROIStartY, ROIStopY);
    case 'Guppy'
        myimages = getVideoGuppy(exposureTime, ROIStartX, ROIStopX, ROIStartY, ROIStopY);
    case 'Stingray'
        myimages = getVideoStingray(exposureTime, ROIStartX, ROIStopX, ROIStartY, ROIStopY);
        size(myimages)
end

function handlesfinal = acquire(handles)
global temp frames view camera
    if not(temp.acquiring)
        temp.acquiring = 1;
        oldsize = size(temp.N);
        switch camera.take
            case {'OD','OD with dark'}
                if strcmp(camera.take,'OD with dark')
                    takedark = 1;
                else
                    takedark = 0;
                end
                ready = camera.iscameraready;
                while (ready)
                    rr = camera.roirect; x1 = rr(1); x2 = rr(2); y1 = rr(3); y2 = rr(4);
                    trig = camera.externaltrigger;
                    if trig
                        if takedark
                            newimages = getFrames(3,camera.exposuretime,1,0,x1,x2,y1,y2);
                        else
                            newimages = getFrames(2,camera.exposuretime,1,0,x1,x2,y1,y2);
                        end
                        if (size(newimages,3)==3) && takedark
                            frames.image = newimages(:,:,1);
                            frames.bright = newimages(:,:,2);
                            frames.darkimage = newimages(:,:,3);
                            frames.darkbright = 0;
                        elseif (size(newimages,3)==2) && not(takedark)
                            frames.image = newimages(:,:,1);
                            frames.bright = newimages(:,:,2);
                        else
                            ready = 0;
                        end
                    else
                        newimages = getFrames(2,camera.exposuretime,0,1,x1,x2,y1,y2);
                        if takedark
                            darkimage = getFrames(1,camera.exposuretime,0,0,x1,x2,y1,y2);
                        else
                            darkimage = 0;
                        end
                        if (size(newimages,3) == 2) && not(isequal(darkimage,0) && takedark)
                            frames.image = newimages(:,:,1);
                            frames.bright = newimages(:,:,2);
                            if takedark
                                frames.darkimage = darkimage(:,:,1);
                                frames.darkbright = 0;
                            end
                        else
                            ready = 0;
                        end
                    end
                    if ready
						savecamera();
                        calc();
                        handles = redraw(handles);
                        handles = analyze(handles);
                        handles = batchstep(handles);
                    end
                    ready = ready && camera.iscameraready && camera.continuous && temp.acquiring;
                end
            case 'video'
                rr = camera.roirect; x1 = rr(1); x2 = rr(2); y1 = rr(3); y2 = rr(4);
                ready = camera.iscameraready;
                while (ready)
                    trig = camera.externaltrigger;
                    newimage = getVideo(camera.exposuretime,x1,x2,y1,y2);
                    if not(isequal(newimage,0))
                        frames.image = newimage;
%                         view.zmax = max(max(temp.z));
%                         view.zmin = min(min(temp.z));
%                         savezoom();
                        handles = redraw(handles);
                    else
                        ready = 0;
                    end
                    ready = ready && camera.iscameraready && camera.continuous && temp.acquiring;
                end
            case 'dark'
                rr = camera.roirect; x1 = rr(1); x2 = rr(2); y1 = rr(3); y2 = rr(4);
                ready = camera.iscameraready;
                while (ready)
                    trig = camera.externaltrigger;
                    newimage = getFrames(1,camera.exposuretime,trig,not(trig),x1,x2,y1,y2);
                    if not(isequal(newimage,0))
                        frames.darkimage = newimage;
                        frames.darkbright = 0;
                        handles = redraw(handles);
                    else
                        ready = 0;
                    end
                    ready = ready && camera.iscameraready && camera.continuous && temp.acquiring;
                end
            case 'FT'
                ready = camera.iscameraready;
                while (ready)
                    rr = camera.roirect; x1 = rr(1); x2 = rr(2); y1 = rr(3); y2 = rr(4);
                    trig = camera.externaltrigger;
                    if trig
                        newimages = getFT(6,2,camera.exposuretime,1,0,x1,x2,y1,y2);      
                        if not(isequal(newimages,0))
                            frames.image = newimages(:,:,1);
                            frames.bright = newimages(:,:,2);
                            frames.darkimage = newimages(:,:,3);
                            frames.darkbright = newimages(:,:,6);
                        else
                            ready = 0;
                        end
                    else
                        newimages = getFT(2,2,camera.exposuretime,0,1,x1,x2,y1,y2);
                        darkframes = getFT(2,2,camera.exposuretime,0,0,x1,x2,y1,y2);
                        if (size(newimages,3)==2) && (size(darkframes,3)==2)
                            frames.image = newimages(:,:,1);
                            frames.bright = newimages(:,:,2);
                            frames.darkimage = darkframes(:,:,1);
                            frames.darkbright = darkframes(:,:,2);
                        else
                            ready = 0;
                        end
                    end
                    if ready
                        savecamera();
                        calc();
                        if any(oldsize ~= size(temp.N))
                            view.rect = [ 1 1 1 1 ];
                        end
                        handles = redraw(handles);
                        handles = analyze(handles);
                        handles = batchstep(handles);
                    end
                    ready = ready && camera.iscameraready && camera.continuous && temp.acquiring;
                end
            case {'fluorescence','fluorescence with dark'}
                if strcmp(camera.take,'fluorescence with dark')
                    takedark = 1;
                else
                    takedark = 0;
                end
                ready = camera.iscameraready;
                while (ready)
                    rr = camera.roirect; x1 = rr(1); x2 = rr(2); y1 = rr(3); y2 = rr(4);
                    trig = camera.externaltrigger;
                    if trig
%                         newimages = getFrames(2,camera.exposuretime,1,0,x1,x2,y1,y2);
%                         if (size(newimages,3)==2) && not(isequal(newimages,0))
%                             frames.image = newimages(:,:,1);
%                             frames.darkimage = newimages(:,:,2);
%                             frames.bright = 0;
%                             frames.darkbright = 0;
%                         else
%                             ready = 0;
%                         end
                        if takedark
                            newimages = getFrames(2,camera.exposuretime,1,0,x1,x2,y1,y2);
                            if (size(newimages,3)==2) && not(isequal(newimages,0))
                                frames.image = newimages(:,:,1);
                                frames.darkimage = newimages(:,:,2);
                                frames.bright = 0;
                                frames.darkbright = 0;
                            else
                                ready = 0;
                            end
                        else
                        newimages = getFrames(1,camera.exposuretime,1,0,x1,x2,y1,y2);
                        if (size(newimages,3)==1) && not(isequal(newimages,0))
                            frames.image = newimages(:,:,1);
%                             frames.darkimage = newimages(:,:,2);
                            frames.bright = 0;
                            frames.darkbright = 0;
                        else
                            ready = 0;
                        end
                        end
                    else
                        if takedark
                            newimage = getFrames(1,camera.exposuretime,0,1,x1,x2,y1,y2);
                            darkimage = getFrames(1,camera.exposuretime,0,0,x1,x2,y1,y2);
                            if not(isequal(newimage,0)) && not(isequal(darkimage,0))
                                frames.image = newimage;
                                frames.darkimage = darkimage;
                                frames.bright = 0;
                                frames.darkbright = 0;
                            else
                                ready = 0;
                            end
                    else
                        newimage = getFrames(1,camera.exposuretime,0,1,x1,x2,y1,y2);
%                         darkimage = getFrames(1,camera.exposuretime,0,0,x1,x2,y1,y2);
                        if not(isequal(newimage,0)) % && not(isequal(darkimage,0))
                            frames.image = newimage;
%                             frames.darkimage = darkimage;
                            frames.bright = 0;
                            frames.darkbright = 0;
                        else
                            ready = 0;
                        end
                    end
                    end
                    if ready
                        savecamera();
                        calc();
                        handles = redraw(handles);
                        handles = analyze(handles);
                        handles = batchstep(handles);
                    end
                    ready = ready && camera.iscameraready && camera.continuous && temp.acquiring;
                end
        end
    end
    temp.acquiring = 0;
    handlesfinal = handles;


%% Update functions

function handlesfinal = updatetext(handles)
global temp frames analysis atom
    x = analysis.crosshair(1); y = analysis.crosshair(2); ps = analysis.pixelsize;
    analysis.chdata = struct;
    
    if (x > 0) && (y > 0)
        analysis.chdata.x_px = x;
        analysis.chdata.y_px = y;
        analysis.chdata.x_um = x*ps;
        analysis.chdata.y_um = y*ps;
        
        if all(size(frames.image) >= [ y x ])
            analysis.chdata.image = frames.image(y,x);
        end
        if all(size(frames.bright) >= [ y x ])
            analysis.chdata.bright = frames.bright(y,x);
        end
        if all(size(frames.darkimage) >= [ y x ])
            analysis.chdata.darkimage = frames.darkimage(y,x);
        end
        if all(size(frames.darkbright) >= [ y x ])
            analysis.chdata.darkbright = frames.darkbright(y,x);
        end
        if all(size(temp.OD) >= [ y x ])
            analysis.chdata.OD = temp.OD(y,x);
        end
        if all(size(temp.N) >= [ y x ])
            analysis.chdata.N = temp.N(y,x);
            analysis.chdata.Nerr = sqrt(temp.Nvar(y,x));
        end
        if all(size(frames.bright) >= [ y x ]) && (analysis.pulselength > 0)
            if all(size(frames.darkbright) >= [ y x ])
                bright = frames.bright(y,x) - frames.darkbright(y,x);
            elseif all(size(frames.darkimage) >= [ y x ])
                bright = frames.bright(y,x) - frames.darkimage(y,x);
            else
                bright = frames.bright(y,x);
            end
            analysis.chdata.sat = (2*analysis.crosssection/(ps^2*analysis.gain* ...
                analysis.efficiency*analysis.linewidth*analysis.pulselength))*bright;
        end
            
                
    end
    
    [label1] = displaystruct(analysis.batchdata);
    [label2] = displaystruct(analysis.intdata);
%     for i=1:length(analysis.intdata)
%         [label2] = [ label2 displaystruct(analysis.intdata(i)) ];
%     end
    [label3] = displaystruct(analysis.fitdata);
    [label4] = displaystruct(analysis.chdata);
    
    label = [label1 label2 label3 label4];
    
    set(handles.text_label,'String',label);

    % Set menu for big display

    handles.menu_BigDisplay = uicontextmenu('Parent',handles.figure1);
    set(handles.text_bigdisplay,'UIContextMenu',handles.menu_BigDisplay);
    
    mydata=vertcombinestructs({analysis.batchdata,analysis.intdata, ...
            analysis.fitdata,analysis.chdata});
    fields=fieldnames(mydata);
    
    for i=1:length(fields)   
        uimenu(handles.menu_BigDisplay,'Label',fields{i}, ...
            'Callback',{@setbigdisplay,fields{i}});
    end
    
    % Set big display
    if length(mydata) >= analysis.currentfitrect
        if isfield(mydata(analysis.currentfitrect),temp.bigdisplay)
            display = sprintf('%0.3G',getfield(mydata(analysis.currentfitrect),temp.bigdisplay));
            if ispc
                display = strrep(display,'E+00','E+0');
            end
            set(handles.text_bigdisplay,'String',display);
        else
            set(handles.text_bigdisplay,'String','');
        end
    else
         set(handles.text_bigdisplay,'String','');
    end
    
    handlesfinal = handles;
    
function handlesfinal = redraw(handles)
global temp frames camera analysis view camerata atom atomdata
    switch view.current
        case 'OD'
            view.zmax = view.ODmax;
            view.zmin = view.ODmin;
            temp.z = temp.OD;
        case 'image'
            view.zmax = view.imagemax;
            view.zmin = view.imagemin;
            temp.z = frames.image;
        case 'bright'
            view.zmax = view.brightmax;
            view.zmin = view.brightmin;
            temp.z = frames.bright;
        case 'darkimage'
            view.zmax = view.darkimagemax;
            view.zmin = view.darkimagemin;
            temp.z = frames.darkimage;
        case 'darkbright'
            view.zmax = view.darkbrightmax;
            view.zmin = view.darkbrightmin;
            temp.z = frames.darkbright;
        case 'N'
            view.zmax = view.Nmax;
            view.zmin = view.Nmin;
            temp.z = temp.N;
    end

    
    set(handles.edit_zmax,'String',view.zmax);
    set(handles.edit_zmin,'String',view.zmin);
    
    vr = view.rect;
    xmin = vr(1); xmax = vr(2); ymin = vr(3); ymax = vr(4);
    
    if (xmax > size(temp.z,2)) || (xmax <= xmin)
        xmax = size(temp.z,2);
    end
    if (xmin < 1) || (xmax <= xmin)
        xmin = 1;
    end
    if (ymax > size(temp.z,1)) || (ymax <= ymin)
        ymax = size(temp.z,1);
    end
    if (ymin < 1) || (ymax <= ymin)
        ymin = 1;
    end
    
    view.rect = [ xmin xmax ymin ymax ];
    
    if view.autoscale
        view.zmax = max(max(temp.z(ymin:ymax,xmin:xmax)));
        view.zmin = min(min(temp.z(ymin:ymax,xmin:xmax)));
        if (view.zmin == view.zmax)
            view.zmin = view.zmax - 1;
        end
        savezoom();
    end
    
    % Set the correct popupmenus and edit boxes, in case they changed (e.g., on file open or app start)

    set(handles.edit_zmax,'String',sprintf('%0.4g',view.zmax));
    set(handles.edit_zmin,'String',sprintf('%0.4g',view.zmin));
    set(handles.edit_TOF,'String',sprintf('%0.4g',analysis.TOF));
    set(handles.edit_exposuretime,'String',sprintf('%0.4g',camera.exposuretime));
    set(handles.edit_pulselength,'String',sprintf('%0.4g',camera.pulselength));
    set(handles.edit_crosssection,'String',sprintf('%0.4g',atom.crosssection));
    set(handles.edit_scatteringrate,'String',sprintf('%0.4g',atom.scatteringrate));
    set(handles.edit_tilt,'String',sprintf('%0.4g',analysis.tilt*180/pi));
    
    if strcmp(camera.take,'fluorescence')
        set(handles.edit_crosssection,'Enable','off')
        set(handles.edit_scatteringrate,'Enable','on')
    else
        set(handles.edit_crosssection,'Enable','on');
        set(handles.edit_scatteringrate,'Enable','off')
    end
    
    if analysis.fixtilt
        set(handles.edit_tilt,'Enable','on');
    else
        set(handles.edit_tilt,'Enable','off');
    end
    
    set(handles.pushbutton_start,'Value',temp.acquiring);
    set(handles.togglebutton_stripchart,'Value',temp.showstripchart);
    set(handles.togglebutton_fullscreen,'Value',temp.fullscreen);
    
    set(handles.checkbox_analyze,'Value',temp.analyzeonopen);
    set(handles.checkbox_fixtilt,'Value',analysis.fixtilt);
    set(handles.checkbox_externaltrig,'Value',camera.externaltrigger);
    set(handles.checkbox_continuous,'Value',camera.continuous);
    set(handles.checkbox_autosaveframes,'Value',temp.saveframes);
    set(handles.checkbox_multiplefitrects,'Value',analysis.multiplefitrects);
    set(handles.checkbox_autoscale,'Value',view.autoscale);
    set(handles.pushbutton_startbatch,'Value',temp.batchrunning);
    
    if not(isfield(analysis,'multiplefitrects')) % backwards compatibility
        analysis.multiplefitrects = 0;
    end

    contents = get(handles.popupmenu_analysis,'String');
    set(handles.popupmenu_analysis,'Value',find(strcmp(analysis.type,contents)));

    contents = get(handles.popupmenu_cameratake,'String');
    set(handles.popupmenu_cameratake,'Value',find(strcmp(camera.take,contents)));

    contents = get(handles.popupmenu_camera,'String');
    set(handles.popupmenu_camera,'Value',find(strcmp(camera.type,contents)));
    
    contents = get(handles.popupmenu_colormap,'String');
    set(handles.popupmenu_colormap,'Value',find(strcmp(view.colormap,contents)));
    
    set(handles.popupmenu_atomdata,'String',{ atomdata.name });
    set(handles.popupmenu_atomdata,'Value',find(strcmp(atom.name,{atomdata.name})));
    
%     choices = get(handles.uipanel_picturechoice,'Children');
%     ind = find(strcmp(get(choices,'String'),camera.take));
%     set(choices(ind),'Value',1);
    
    choices = get(handles.uipanel_fitrect,'Children');
    
    if analysis.multiplefitrects
        for i=1:length(choices)
            if strcmp(get(choices(i),'Style'),'radiobutton')
                 set(choices(i),'Enable','on');
            end
        end
        if analysis.currentfitrect > size(analysis.fitrect,1)
            analysis.fitrect(analysis.currentfitrect,:) = [ 0 0 0 0 ];
        end
    else
        for i=1:length(choices)
            if strcmp(get(choices(i),'Style'),'radiobutton')
                 set(choices(i),'Enable','off');
            end
        end
        analysis.currentfitrect = 1;
        analysis.fitrect = analysis.fitrect(1,:);
    end

    ind = find(str2double(get(choices,'String')) == analysis.currentfitrect);
    set(choices(ind),'Value',1);
    
    if strcmp(view.colormap,'camerata')
        colormap(camerata);
    else
        colormap(view.colormap);
    end
    
    % Draw axes    
    if (xmax == 1) && (ymax == 1)
        cla(handles.axes_full);
        cla(handles.axes_x);
        cla(handles.axes_y);
        cla(handles.axes_fit);
        cla(handles.axes_fitregion);
    else
        set(0,'CurrentFigure',handles.figure1);
        set(handles.figure1,'CurrentAxes',handles.axes_full);
%         axes(handles.axes_full);
        imagesc(temp.z,[view.zmin,view.zmax]);
        axis xy;
        axis equal;
        axis(handles.axes_full, [xmin xmax ymin ymax]);
        caxis(handles.axes_full, [view.zmin view.zmax]);

        
        % Draw rectangles and crosshairs
        for i=1:size(analysis.fitrect,1)
            fr = analysis.fitrect(i,:); x1 = fr(1); x2 = fr(2); y1 = fr(3); y2 = fr(4);
            if (x2 > x1) && (y2 > y1) && all(size(temp.z) >= [ y2 x2 ])
                if i == analysis.currentfitrect
                    rectangle('Position',[x1,y1,x2-x1,y2-y1],'EdgeColor','w','LineWidth',1.5);
                else
                    rectangle('Position',[x1,y1,x2-x1,y2-y1],'EdgeColor','w','LineWidth',1);
                end
            end
        end


        nr = analysis.normrect; x1 = nr(1); x2 = nr(2); y1 = nr(3); y2 = nr(4);
        if (x2 > x1) && (y2 > y1) && all(size(temp.z) >= [ y2 x2 ])
            rectangle('Position',[x1,y1,x2-x1,y2-y1],'EdgeColor','r');
        end
        
        x = analysis.crosshair(1); y = analysis.crosshair(2);
        if (x > 0) && (y > 0) && all(size(temp.z) >= [ y x ])
            if (xmin <= x) && (x <= xmax)
                line([x x], [1 length(temp.z(:,1))],'Color', 'w');
            end
            if (ymin <= y) && (y <= ymax)
                line([1 length(temp.z(1,:))], [y y], 'Color', 'w');
            end
            analysis.crosshair = [ x y ];

        % Draw line plots
            
           fr = analysis.fitrect(analysis.currentfitrect,:); x1 = fr(1); x2 = fr(2); y1 = fr(3); y2 = fr(4);
            if (x1 < x) && (x < x2) && strcmp(view.current, 'N') && (size(temp.fit,1) == y2-y1+1) && (size(temp.fit,2) == x2-x1+1)
                plot(handles.axes_y,temp.z(:,x),1:length(temp.z(:,1)),temp.fit(:,x-x1),y1:y2);
            else
                plot(handles.axes_y,temp.z(:,x),1:length(temp.z(:,1)));
            end

            if (y1 < y) && (y < y2) && strcmp(view.current, 'N') && (size(temp.fit,1) == y2-y1+1) && (size(temp.fit,2) == x2-x1+1)
                plot(handles.axes_x,1:length(temp.z(1,:)),temp.z(y,:),x1:x2,temp.fit(y-y1,:));
            else
                plot(handles.axes_x,1:length(temp.z(1,:)),temp.z(y,:));
            end
            xlim(handles.axes_y,[view.zmin view.zmax]);
            ylim(handles.axes_y,[ymin ymax]);
            xlim(handles.axes_x,[xmin xmax]);
            ylim(handles.axes_x,[view.zmin view.zmax]);
        else
            analysis.crosshair = [ 0 0 ];
            cla(handles.axes_x);
            cla(handles.axes_y);
        end
            
        %Draw fit plots
        fr = analysis.fitrect(analysis.currentfitrect,:); x1 = fr(1); x2 = fr(2); y1 = fr(3); y2 = fr(4);
        if (x1 >= 1) && (x2 <= size(temp.N,2)) && (x2 > x1) && (y1 >= 1) && (y2 <= size(temp.N,1)) && (y2 > y1)
            set(0,'CurrentFigure',handles.figure1);
            set(handles.figure1,'currentAxes',handles.axes_fitregion);
%             axes(handles.axes_fitregion);
            imagesc(x1:x2,y1:y2,temp.N(y1:y2,x1:x2),[view.Nmin,view.Nmax]);
            axis xy;
            axis equal;
            
            if (size(temp.fit,1) == y2-y1+1) && (size(temp.fit,2) == x2-x1+1)
%                 axes(handles.axes_fit);
                set(0,'CurrentFigure',handles.figure1);
                set(handles.figure1,'currentAxes',handles.axes_fit);
                imagesc(x1:x2,y1:y2,temp.fit,[view.Nmin,view.Nmax]);
                axis xy;
                axis equal;
                plot(handles.axes_sumx,x1:x2,sum(temp.N(y1:y2,x1:x2),1),x1:x2,sum(temp.fit,1));
                plot(handles.axes_sumy,sum(temp.N(y1:y2,x1:x2),2),y1:y2,sum(temp.fit,2),y1:y2);
                
%                 figure(3)
%                 plot(gca,x1:x2,sum(temp.N(y1:y2,x1:x2),1),x1:x2,sum(temp.fit,1));
%                 plot(gca,x1:x2,sum(temp.N(y1:y2,x1:x2),1));
%                 csvwrite('critical2c_0020.csv',[ transpose(sum(temp.N(y1:y2,x1:x2),1)) transpose(sum(temp.fit,1)) ]);
            else
                cla(handles.axes_fit);
                plot(handles.axes_sumx,x1:x2,sum(temp.N(y1:y2,x1:x2),1));
                plot(handles.axes_sumy,sum(temp.N(y1:y2,x1:x2),2),y1:y2);
            end
            axis(handles.axes_sumx,'tight');
            axis(handles.axes_sumy,'tight');
        else
            cla(handles.axes_fitregion);
            cla(handles.axes_fit);
            cla(handles.axes_sumx);
            cla(handles.axes_sumy);
        end
        
        %Draw extra figures
        
        
        
        % Full screen: always figure 3
        if temp.fullscreen
            sfigure(3);
            vr = view.rect;
            x1 = vr(1); x2 = vr(2); y1 = vr(3); y2 = vr(4);
            h = imagesc(temp.z(y1:y2,x1:x2));

            if strcmp(view.colormap,'camerata')
                colormap(camerata);
            else
                colormap(view.colormap);
            end
            axis xy
            axis equal
            caxis([view.zmin view.zmax]);
        end

    end

    handles = updatetext(handles);
    
    handlesfinal = handles;

function savezoom()
global view
    switch view.current
        case 'OD'
            view.ODmax = view.zmax;
            view.ODmin = view.zmin;
        case 'image'
            view.imagemax = view.zmax;
            view.imagemin = view.zmin;
        case 'bright'
            view.brightmax = view.zmax;
            view.brightmin = view.zmin;
        case 'darkimage'
            view.darkimagemax = view.zmax;
            view.darkimagemin = view.zmin;
        case 'darkbright'
            view.darkbrightmax = view.zmax;
            view.darkbrightmin = view.zmin;
        case 'N'
            view.Nmax = view.zmax;
            view.Nmin = view.zmin;
    end

    
%% Analysis functions

function calc()
global analysis
    if strncmp(analysis.take,'fluorescence',12)
        calcFluor();
    else
        calcAbs();
    end

function calcAbs()
global temp frames analysis
    image = double(frames.image);   % they should already be double, but just in case
    bright = double(frames.bright);
    darkimage = double(frames.darkimage);
    darkbright = double(frames.darkbright);
    
    if isequal(darkimage,0);
        darkimage = zeros(size(image));
    end
    if isequal(darkbright,0)
        darkbright = darkimage;
    end
    
    if all(size(image) == size(bright)) && all(size(image) == size(darkimage)) ...
            && (all(size(image) == size(darkbright)))
        top = bright - darkbright;
        bottom = image - darkimage;
        lowintensity = zeros(size(top));
        highintensity = zeros(size(top));
        lowintensityvar = zeros(size(top));
        highintensityvar = zeros(size(top));
        
        nr = analysis.normrect; nx1 = nr(1); nx2 = nr(2); ny1 = nr(3); ny2 = nr(4);
         
        if all([nx1 ny1] > 0) && all([nx2 ny2] > [nx1 ny1]) && all([ny2 nx2] <= size(top))
            coeff = sum(sum(bottom(ny1:ny2,nx1:nx2)))/sum(sum(top(ny1:ny2,nx1:nx2)));
            if coeff < 0 || coeff == Inf || isnan(coeff)
                coeff = 1;
            end
        else
            coeff = 1;
        end
        
        analysis.norm = coeff;
        top = top*coeff;
        
        for n = 1:size(top,1)
            for m = 1:size(top,2)
                if (top(n,m) > 0) && (bottom(n,m) > 0)
                    lowintensity(n,m) = log(top(n,m)/bottom(n,m));
                    highintensity(n,m) = top(n,m) - bottom(n,m);
                    lowintensityvar(n,m) = (1/top(n,m)) + (1/bottom(n,m));
                    highintensityvar(n,m) = top(n,m) + bottom(n,m);
                else
                    lowintensity(n,m) = 0;
                    highintensity(n,m) = 0;
                    lowintensityvar(n,m) = 0;
                    highintensityvar(n,m) = 0;
                end
            end
        end

        if analysis.pulselength == 0
            high = 0;
        else
            high = 2/(analysis.linewidth*analysis.pulselength* ...
                analysis.efficiency*analysis.gain);
        end
        low = (analysis.pixelsize)^2/analysis.crosssection;
        
        temp.OD = lowintensity;
        lowintensity = low*lowintensity;
        lowintensityvar = low^2*lowintensityvar*analysis.gain;
        if analysis.pulselength > 0
            highintensity = high*highintensity;
            highintensityvar = high^2*highintensityvar*analysis.gain;
            temp.N = lowintensity + highintensity;
            temp.Nvar = lowintensityvar + highintensityvar + 4*low*high*analysis.gain;
        else
            temp.N = lowintensity;
            temp.Nvar = lowintensityvar;
        end
    end

function calcFluor()
global temp frames analysis
    if analysis.pulselength > 0
        if isequal(size(frames.image),size(frames.darkimage)) && not(isequal(frames.darkimage,0))
            nr = analysis.normrect; nx1 = nr(1); nx2 = nr(2); ny1 = nr(3); ny2 = nr(4);

            if all([nx1 ny1] > 0) && all([nx2 ny2] > [nx1 ny1]) && all([ny2 nx2] <= size(frames.image))
                coeff = sum(sum(frames.image(ny1:ny2,nx1:nx2)))/sum(sum(frames.darkimage(ny1:ny2,nx1:nx2)));
                if coeff < 0 || coeff == Inf || isnan(coeff)
                    coeff = 1;
                end
            else
                coeff = 1;
            end           
            analysis.norm = coeff;
            
            signal = double(frames.image) - coeff*double(frames.darkimage);
        else
            signal = double(frames.image);
        end

        
        prefactor = (4*pi/analysis.solidangle)*1/(analysis.scatteringrate* ...
            analysis.pulselength*analysis.efficiency*analysis.gain);
        temp.N = prefactor*signal;

        temp.Nvar = abs(temp.N); % I should take three images: atoms, no atoms, and dark
        temp.OD = 0;
    end

function savecamera()
global analysis camera atom
    analysis.atomname = atom.name;
    analysis.pixelsize = camera.pixelsize;
    analysis.efficiency = atom.efficiency;
    analysis.gain = camera.gain;
    analysis.crosssection = atom.crosssection;
    analysis.linewidth = atom.linewidth;
    analysis.pulselength = camera.pulselength;
    analysis.solidangle = camera.solidangle;
    analysis.take = camera.take;
    analysis.scatteringrate = atom.scatteringrate;
    
function handlesfinal = analyze(handles)
global temp frames analysis atom
    analysis.intdata = struct;
    analysis.fitdata = struct;
    
    for i=1:size(analysis.fitrect,1)
        fr = analysis.fitrect(i,:);
        x1 = fr(1); x2 = fr(2); y1 = fr(3); y2 = fr(4);
        ps = analysis.pixelsize;
        [ ymax xmax ] = size(temp.N);

        if (x2 > x1) && (y2 > y1) && (x1 >= 1) && (x2 <= xmax) && (y1 >= 1) && (y2 <= ymax)
            fitregion = temp.N(y1:y2,x1:x2);
            if not(isequal(frames.image,0) || isequal(frames.darkimage,0)) 
                image = frames.image(y1:y2,x1:x2) - frames.darkimage(y1:y2,x1:x2);
            else
                image = 0;
            end
            if any(size(frames.darkbright) < [ y2 x2 ]) && not(isequal(frames.bright,0))
                bright = frames.bright(y1:y2,x1:x2) - frames.darkimage(y1:y2,x1:x2);
            elseif not(isequal(frames.bright,0))
                bright = frames.bright(y1:y2,x1:x2) - frames.darkbright(y1:y2,x1:x2);
            else
                bright = 0;
            end
            
            bright = analysis.norm*bright;

            if analysis.fixtilt
                params = guess(fitregion,analysis.tilt);
            else
                params = guess(fitregion);
            end
            analysis.intdata(i).xcom = (params(1) + x1)*ps;
            analysis.intdata(i).ycom = (params(2) + y1)*ps;
            analysis.intdata(i).SDx = params(3)*ps;
            analysis.intdata(i).SDy = params(4)*ps;
            analysis.intdata(i).SDtilt = params(7)*180/pi; %degrees
            analysis.intdata(i).min = params(5);
            analysis.intdata(i).max = params(6);
            analysis.intdata(i).NSum = sum(sum(fitregion));
            analysis.intdata(i).NSumerr = sqrt(sum(sum(temp.Nvar(y1:y2,x1:x2))));
            analysis.intdata(i).imagesum = sum(sum(image));
            analysis.intdata(i).brightsum = sum(sum(bright));
%             analysis.intdata(i).NSumlow = (ps^2/analysis.crosssection)*sum(sum(temp.OD(y1:y2,x1:x2)));
%             analysis.intdata(i).NSumhigh =  analysis.intdata(i).NSum - analysis.intdata(i).NSumlow;
            if (analysis.TOF > 0)
                analysis.intdata(i).Tx = 0.0105*(params(3)*ps/analysis.TOF)^2; % microkelvin
                analysis.intdata(i).Ty = 0.0105*(params(4)*ps/analysis.TOF)^2; % microkelvin
            end
            if (analysis.pulselength > 0) && not(isequal(bright,0))
                analysis.intdata(i).avg_s = (2*analysis.crosssection/(ps^2* ...
                    analysis.gain*analysis.efficiency*analysis.linewidth* ...
                    analysis.pulselength))*mean(mean(bright));
            end
            analysis.intdata(i).gain = analysis.gain;
            analysis.intdata(i).efficiency = analysis.efficiency;
            analysis.intdata(i).crosssection = atom.crosssection;
            analysis.intdata(i).linewidth = atom.linewidth;
            analysis.intdata(i).pulselength = analysis.pulselength;
            analysis.intdata(i).TOF = analysis.TOF;
%             analysis.intdata(i).pixels = (y2-y1+1)*(x2-x1+1);
            analysis.intdata(i).rectange_x1 = x1;
            analysis.intdata(i).rectange_x2 = x2;
            analysis.intdata(i).rectange_y1 = y1;
            analysis.intdata(i).rectange_y2 = y2;
            analysis.intdata(i).atom = atom.name;
            handles = updatetext(handles);
        end
    end

    if (size(analysis.fitrect,1) >= analysis.currentfitrect)
        fr = analysis.fitrect(analysis.currentfitrect,:); x1 = fr(1); x2 = fr(2); y1 = fr(3); y2 = fr(4); ps = analysis.pixelsize;
        [ ymax xmax ] = size(temp.N);

        if (x2 > x1) && (y2 > y1) && (x1 >= 1) && (x2 <= xmax) && (y1 >= 1) && (y2 <= ymax)
            fitregion = temp.N(y1:y2,x1:x2);

            analysis.fitdata.fittype = analysis.type;
            switch analysis.type
                case 'None'
                    temp.fit = 0;
                    cla(handles.axes_fit);
                case 'Gauss1D'
                    set(0,'CurrentFigure',handles.figure1);
                    set(handles.figure1,'currentAxes',handles.axes_full);
%                     axes(handles.axes_fit);

                    initial = guess(fitregion,0);
                    fitparams = gauss1dcoupledfit(transpose(sum(fitregion,1)),sum(fitregion,2),initial);
                    temp.fit = gauss2d(fitparams,fitregion);

                    analysis.fitdata.x0 = (fitparams(1) + x1)*ps;
                    analysis.fitdata.y0 = (fitparams(2) + y1)*ps;
                    analysis.fitdata.sigma_x = fitparams(3)*ps;
                    analysis.fitdata.sigma_y = fitparams(4)*ps;
                    analysis.fitdata.background = fitparams(5);
                    analysis.fitdata.peak = fitparams(6);
                    analysis.fitdata.NFit = sum(sum(temp.fit-fitparams(5)));
                    if (analysis.TOF > 0)
                        analysis.fitdata(i).fitTx = 0.0105*(fitparams(3)*ps/analysis.TOF)^2; % microkelvin
                        analysis.fitdata(i).fitTy = 0.0105*(fitparams(4)*ps/analysis.TOF)^2; % microkelvin
                   end
                case 'Gauss2D'
                    set(0,'CurrentFigure',handles.figure1);
                    set(handles.figure1,'currentAxes',handles.axes_fit);
%                     axes(handles.axes_fit);

                    if analysis.fixtilt
                        initial = guess(fitregion,analysis.tilt);
                        fitparams = gauss2dfit(fitregion,initial,1);
                    else
                        initial = guess(fitregion);
                        fitparams = gauss2dfit(fitregion,initial,0);
                    end
                    temp.fit = gauss2d(fitparams,fitregion);

                    analysis.fitdata.x0 = (fitparams(1) + x1)*ps;
                    analysis.fitdata.y0 = (fitparams(2) + y1)*ps;
                    analysis.fitdata.sigma_x = fitparams(3)*ps;
                    analysis.fitdata.sigma_y = fitparams(4)*ps;
                    analysis.fitdata.sigma_tilt = fitparams(7)*180/pi;
                    analysis.fitdata.background = fitparams(5);
                    analysis.fitdata.peak = fitparams(6);
                    analysis.fitdata.NFit = sum(sum(temp.fit-fitparams(5)));                

                    if (analysis.TOF > 0)
                        analysis.fitdata.fitTx = 0.0105*(fitparams(3)*ps/analysis.TOF)^2; % microkelvin
                        analysis.fitdata.fitTy = 0.0105*(fitparams(4)*ps/analysis.TOF)^2; % microkelvin
                    end
                case 'Parabolic'
                    set(0,'CurrentFigure',handles.figure1);
                    set(handles.figure1,'currentAxes',handles.axes_fit);
%                     axes(handles.axes_fit);

                    if analysis.fixtilt
                        initial = guess(fitregion,analysis.tilt);
                        fitparams = parabolic2dfit(fitregion,initial,1);
                    else
                        initial = guess(fitregion);
                        fitparams = parabolic2dfit(fitregion,initial,0);
                    end
                    temp.fit = parabolic2d(fitparams,fitregion);

                    analysis.fitdata.x0 = (fitparams(1) + x1)*ps;
                    analysis.fitdata.y0 = (fitparams(2) + y1)*ps;
                    analysis.fitdata.Rx = fitparams(3)*ps;
                    analysis.fitdata.Ry = fitparams(4)*ps;
                    analysis.fitdata.Rtilt = fitparams(7)*180/pi;
                    analysis.fitdata.background = fitparams(5);
                    analysis.fitdata.peak = fitparams(6);
                    analysis.fitdata.NFit = sum(sum(temp.fit-fitparams(5)));                

               case 'Bimodal'
                    set(0,'CurrentFigure',handles.figure1);
                    set(handles.figure1,'currentAxes',handles.axes_fit);
%                     axes(handles.axes_fit);

                    if analysis.fixtilt
                        tempinitial = guess(fitregion,analysis.tilt);
                    else
                        tempinitial = guess(fitregion);
                    end

                    x0 = tempinitial(1);            y0 = tempinitial(2);
                    sigx = tempinitial(3);          sigy = tempinitial(4);
                    Rx = tempinitial(3);            Ry = tempinitial(4);
                    back = tempinitial(5);
                    peak_G = 0.5*tempinitial(6);    peak_P = 0.5*tempinitial(6);
                    tilt = tempinitial(7);
                    initial = [ x0 y0 sigx sigy Rx Ry back peak_G peak_P tilt ];

                    fitparams = bimodal2dfit(fitregion,initial,analysis.fixtilt);
                    temp.fit = bimodal2d(fitparams,fitregion);

                    thermal = gauss2d([fitparams(1:4) 0 fitparams(8) fitparams(10)],fitregion);
                    condensed = parabolic2d([fitparams(1:2) fitparams(5:6) 0 fitparams(9:10)],fitregion);

                    analysis.fitdata.x0 = (fitparams(1) + x1)*ps;
                    analysis.fitdata.y0 = (fitparams(2) + y1)*ps;
                    analysis.fitdata.sigma_x = fitparams(3)*ps;
                    analysis.fitdata.sigma_y = fitparams(4)*ps;
                    analysis.fitdata.Rx = fitparams(5)*ps;
                    analysis.fitdata.Ry = fitparams(6)*ps;
                    analysis.fitdata.fittilt = fitparams(10)*180/pi;
                    analysis.fitdata.background = fitparams(7);
                    analysis.fitdata.peak_G = fitparams(8);
                    analysis.fitdata.peak_P = fitparams(9);
                    analysis.fitdata.NFit = sum(sum(temp.fit-fitparams(7)));
                    analysis.fitdata.Nthermal = sum(sum(thermal));
                    analysis.fitdata.Ncond = sum(sum(condensed));

                    if (analysis.TOF > 0)
                        analysis.fitdata.fitTx = 0.0105*(fitparams(3)*ps/analysis.TOF)^2; % microkelvin
                        analysis.fitdata.fitTy = 0.0105*(fitparams(4)*ps/analysis.TOF)^2; % microkelvin
                    end
                case 'Noise'
                    temp.fit = 0;
                    cla(handles.axes_fit);
                    fr = analysis.fitrect(analysis.currentfitrect,:);
                    x1 = fr(1); x2 = fr(2); y1 = fr(3); y2 = fr(4);

                    %this should be unnecessary in future versions
                    if (x2 > x1) && (y2 > y1) && all(size(temp.N) >= [y2 x2])
                        if isequal(frames.darkimage,0)
                            darkimage = zeros(size(frames.image));
                        else
                            darkimage = frames.darkimage;
                        end

                        if isequal(frames.darkbright,0)
                            darkbright = frames.darkimage;
                        else
                            darkbright = frames.darkbright;
                        end
                    end

                    image = reshape(frames.image(y1:y2,x1:x2) - darkimage(y1:y2,x1:x2),(y2-y1+1)*(x2-x1+1),1);
                    bright = reshape(frames.bright(y1:y2,x1:x2) - darkbright(y1:y2,x1:x2),(y2-y1+1)*(x2-x1+1),1);
                    sk = (bright-image)./sqrt(analysis.gain*(bright+image)); % something like a normalized Skellam distribution
                    Nlist = reshape(temp.N(y1:y2,x1:x2),(y2-y1+1)*(x2-x1+1),1);
                    Nvarlist = reshape(temp.Nvar(y1:y2,x1:x2),(y2-y1+1)*(x2-x1+1),1);
                    analysis.fitdata.mean_image = mean(image);
                    analysis.fitdata.mean_bright = mean(bright);
                    analysis.fitdata.mean_sk = mean(sk);
                    analysis.fitdata.var_sk = var(sk);
                    analysis.fitdata.mean_N = mean(Nlist);
                    analysis.fitdata.SD_N = std(Nlist);
                    analysis.fitdata.exp_SD_N = sqrt(mean(Nvarlist));
                case 'Double well'
                    if length(analysis.intdata) > 1
                        left = analysis.intdata(1).NSum;
                        right = analysis.intdata(2).NSum;
                        errleft = analysis.intdata(1).NSumerr;
                        errright = analysis.intdata(2).NSumerr;
                        analysis.fitdata.NSumleft = left;
                        analysis.fitdata.NSumright = right;
                        analysis.fitdata.NSumerrleft = errleft;
                        analysis.fitdata.NSumerrright = errright;
                        analysis.fitdata.delta = left-right;
                        analysis.fitdata.tot = left+right;
                        analysis.fitdata.ratio = (left-right)/sqrt(left+right);
                        analysis.fitdata.imbalance = (left-right)/(left+right);
                        analysis.fitdata.imagingnoiseratio = sqrt((errleft^2+errright^2)/(left+right));
                    end
            end
        end
    end
    handles = redraw(handles);
    handlesfinal = handles;

    
    % guess(z) fits a tilt, guess(z,theta) returns the standard deviation
    % with a fixed tilt 'theta'
function params = guess(z,varargin)
    [sizey,sizex] = size(z);
    [x,y]=meshgrid(1:sizex,1:sizey);
    
    % first moments
    x1 = sum(sum(x.*z))/sum(sum(z));
    y1 = sum(sum(y.*z))/sum(sum(z));
    
    % second moments
    x2 = sum(sum(x.*x.*z))/sum(sum(z));
    y2 = sum(sum(y.*y.*z))/sum(sum(z));
    
    xy = sum(sum(x.*y.*z))/sum(sum(z));
    
    S=[[ x2-x1.^2  xy-x1.*y1 ];[ xy-x1.*y1  y2-y1.^2]];
    
    if length(varargin) > 0
        tilt = varargin{1};
        R = [[ cos(tilt) sin(tilt) ];[ -sin(tilt) cos(tilt) ]];
        D = R\S*R;
    else
        [V,D] = eig(S);
        tilt = -atan(V(2,1)/V(1,1));
        if (tilt > pi/4)
            flip = [[0 1];[1 0]];
            tilt = tilt - pi/2;
            D = flip*D*flip;
        elseif (tilt < -pi/4)
            flip = [[0 1];[1 0]];
            tilt = tilt + pi/2;
            D = flip*D*flip;
        end
    end
    sigx = sqrt(abs(D(1,1))); sigy = sqrt(abs(D(2,2)));
    
    background = min(min(z));
    peak = max(max(z)) - background;
    
    params = [x1 y1 sigx sigy background peak tilt];
    
    % Normal 2D Gaussian with tilt
function out = gauss2d(params,base)
    x0 = params(1); y0 = params(2); sigx = params(3); sigy = params(4);
    background = params(5); peak = params(6);
    if (length(params) > 6)
        theta = params(7);
    else
        theta = 0;
    end
    
    [sizey,sizex] = size(base);
    [x,y]=meshgrid(1:sizex,1:sizey);
    out = background + peak*exp(-.5*((((x-x0)*cos(theta)-(y-y0)*sin(theta))/sigx).^2 ...
        +(((y-y0)*cos(theta)+(x-x0)*sin(theta))/sigy).^2));
    
    %Fit a guassian with or without varying the tilt. The angle is passed
    %through params
function soln = gauss2dfit(z,params,fixtilt)
    options=optimset('Display','off','TolFun',1e-6,'TolX',1e-6);
    if fixtilt
        theta = params(7);
        soln = fminsearch(@errorfuncgauss2d,params(1:6),options,z,theta);
        soln = [soln theta];
    else
        soln=fminsearch(@errorfuncgauss2d,params,options,z);
    end
    
function sse = errorfuncgauss2d(params, z, varargin)
    if length(varargin) > 0
        params(7) = varargin{1};
    end
    fit=gauss2d(params,z);
    sse=sum(sum((fit-z).^2));
    
    % Coupled 1D Gaussians: fit two line plots with the same peak and
    % background values

function out = gauss1d(params,base)
    x0 = params(1); sig = params(2); background = params(3); peak = params(4);
    x = transpose(1:size(base));
    out = background + peak*exp(-.5*((x-x0)/sig).^2);

function soln = gauss1dcoupledfit(x,y,params)
    options=optimset('Display','off','TolFun',1e-6,'TolX',1e-6);
    soln=fminsearch(@errorfuncgauss1dcoupled,params,options,x,y);
    
function sse = errorfuncgauss1dcoupled(params, x,y)
    x0 = params(1); y0 = params(2); sigx = params(3); sigy = params(4);
    background = params(5); peak = params(6);
    
    xlower = x0/(sigx*sqrt(2));
    xupper = (x0-length(x))/(sigx*sqrt(2));
    xint = sigx*sqrt(pi/2)*(erf(xlower)-erf(xupper));
    sizex = length(x);
    
    ylower = y0/(sigy*sqrt(2));
    yupper = (y0-length(y))/(sigy*sqrt(2));
    yint = sigy*sqrt(pi/2)*(erf(ylower)-erf(yupper));
    sizey = length(y);
    
    fitx = gauss1d([x0 sigx background*sizey peak*yint],x);
    fity = gauss1d([y0 sigy background*sizex peak*xint],y);
    
    sse=sum(sum((fitx-x).^2)) + sum(sum((fity-y).^2));
    
function out = parabolic2d(params,base)
    x0 = params(1); y0 = params(2); Rx = params(3); Ry = params(4);
    background = params(5); peak = params(6);
    if (length(params) > 6)
        theta = params(7);
    else
        theta = 0;
    end
    
    [sizey,sizex] = size(base);
    [x,y]=meshgrid(1:sizex,1:sizey);
    out = background + peak*max(1-(((x-x0)*cos(theta)-(y-y0)*sin(theta))/Rx).^2 ...
        - (((y-y0)*cos(theta)+(x-x0)*sin(theta))/Ry).^2,0).^(3/2);

function soln = parabolic2dfit(z,params,fixtilt)
    options=optimset('Display','off','TolFun',1e-6,'TolX',1e-6);
    if fixtilt
        theta = params(7);
        soln = fminsearch(@errorfuncparabolic2d,params(1:6),options,z,theta);
        soln = [soln theta];
    else
        soln=fminsearch(@errorfuncparabolic2d,params,options,z);
    end
    
function sse = errorfuncparabolic2d(params, z, varargin)
    if length(varargin) > 0
        params(7) = varargin{1};
    end
    fit=parabolic2d(params,z);
    sse=sum(sum((fit-z).^2));

function out = bimodal2d(params,base)
    x0 = params(1);     y0 = params(2);
    sigx = params(3);   sigy = params(4);
    Rx = params(5);     Ry = params(6);
    back = params(7);
    peak_G = params(8); peak_P = params(9);
    if (length(params) > 9)
        theta = params(10);
    else
        theta = 0;
    end
    out = back + gauss2d([x0 y0 sigx sigy 0 peak_G theta],base) + ...
        parabolic2d([x0 y0 Rx Ry 0 peak_P theta],base);
    
function soln = bimodal2dfit(z,params,fixtilt)
    options=optimset('Display','off','TolFun',1e-6,'TolX',1e-6);
    if fixtilt
        theta = params(10);
        soln = fminsearch(@errorfuncbimodal2d,params(1:9),options,z,theta);
        soln = [soln theta];
    else
        soln=fminsearch(@errorfuncbimodal2d,params,options,z);
    end
    
function sse = errorfuncbimodal2d(params, z, varargin)
    if length(varargin) > 0
        params(10) = varargin{1};
    end
    fit=bimodal2d(params,z);
    sse=sum(sum((fit-z).^2));
    

    
%% Save and Batch functions

function handlesfinal = batchstep(handles)
global temp frames analysis view batch camera
    if (temp.batchrunning)
        batch.num = batch.num + 1;
        filename = [ batch.filename '_' sprintf('%04.0f',batch.num) '.mat'];
        if not(isstruct(analysis.batchdata))
            analysis.batchdata = struct;
        end
        analysis.batchdata.runnum = batch.num;
        analysis.batchdata.batchname = batch.filename;
        
        if temp.saveframes
            analysis.batchdata.filename = filename;
            save([ batch.path filename ],'frames','analysis','view','camera','batch');
        end
        

        newbatch=vertcombinestructs({analysis.batchdata,analysis.intdata, ...
            analysis.fitdata,analysis.chdata});

        if (batch.num == 1)
            handles.batchall = newbatch;
        else
            handles.batchall = horzcombinestructs({handles.batchall newbatch});
        end
        
        writestruct( [ batch.path batch.filename '.csv'], handles.batchall);
        mybatch = handles.batchall;
        save([ batch.path batch.filename '.mat' ],'mybatch');
    else
        analysis.batchdata = struct;
    end
    
    % Stripchart: always figure 2
    if temp.showstripchart
%         temp.stripchart1
        points = 30;
        sfigure(2);
        
        mydata=vertcombinestructs({analysis.batchdata,analysis.intdata, ...
            analysis.fitdata,analysis.chdata});
        fields=fieldnames(mydata);
        
        if ishandle(2)
            if isfield(analysis.intdata,'NSum')
                temp.stripchart1(length(temp.stripchart1) + 1) = analysis.intdata(1).NSum;
            else
                temp.stripchart1(length(temp.stripchart1) + 1) = 0;
            end
            
            if isfield(mydata,temp.bigdisplay)
                temp.stripchart2(length(temp.stripchart2) + 1) = mydata.(temp.bigdisplay);
            else
                temp.stripchart2(length(temp.stripchart2) + 1) = 0;
            end
        end
        if length(temp.stripchart1) > points
            temp.stripchart1 = temp.stripchart1(2:(points+1));
            temp.stripchart2 = temp.stripchart2(2:(points+1));
        end
        subplot(2,1,1); plot(temp.stripchart1,'o-','LineWidth',1.5,'Color','r');
        subplot(2,1,2); plot(temp.stripchart2,'*-','LineWidth',1.5);
        v = axis;
        text(.8*(v(2)-v(1))+v(1),0.2*(v(4)-v(3))+v(3),sprintf('%0.4G',mean(temp.stripchart1)));
%         axis([1 points 0 max([ max(temp.stripchart1) 0]) ]);
%         axis([1 points 5E7 5E8 ]);
%         subplot(2,1,2); plot(temp.stripchart2,'.-');
%         axis([1 points 0 max([ max(temp.stripchart2) 0]) ]);
    end
    handles = updatetext(handles);
    handlesfinal = handles;
    
function saveedcam(pathname,filename)
global frames analysis view camera
    save([ pathname filename ],'frames','analysis','view','camera','batch');


    
    
    

% --- Executes on selection change in popupmenu_cameratake.
function popupmenu_cameratake_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_cameratake (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu_cameratake contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_cameratake
global temp camera
    contents = get(hObject,'String');
    camera.take = contents{get(hObject,'Value')};
    if temp.acquiring
        stop();
        handles = acquire(handles);
    end
    handles = redraw(handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_cameratake_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_cameratake (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pushbutton_atom.
function pushbutton_atom_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_atom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global atom atomdata
    tempatomdata = atomdata;
    val = get(handles.popupmenu_atomdata,'Value');
    tempatomdata = rmfield(tempatomdata,'crosssection');
    tempatomdata = rmfield(tempatomdata,'scatteringrate');
    for i=1:length(tempatomdata)
        tempatomdata(i).linewidth = tempatomdata(i).linewidth/(2*pi);
    end
    
    [ atomdata val ] = editstruct(tempatomdata,val);
    for i=1:length(atomdata)
        delta = 2*(atomdata(i).detuning)/(atomdata(i).linewidth);
        atomdata(i).linewidth = 2*pi*atomdata(i).linewidth;
        if atomdata(i).sat > 0
            bottom = 1+atomdata(i).sat+delta^2;
        else
            bottom = 1;
        end

        atomdata(i).crosssection = atomdata(i).sigma0/bottom;
        atomdata(i).scatteringrate = (atomdata(i).linewidth/2)/bottom;
%         atomdata(i)
    end
    
    atom = atomdata(val);
    handles = redraw(handles);

% global camera analysis
% 
%     cr = sprintf('\n');
%     options.WindowStyle='normal';
%     options.Interpreter = 'tex';
%     
%     sigma0 = camera.crosssection;
%     linewidth = camera.linewidth/(2*pi);
%     delta = linewidth*camera.detuning;
%     camera.linewidth
%     camera.scatteringrate
%     sat = (1+4*(camera.detuning)^2)/(camera.linewidth/(2*camera.scatteringrate)-1);
%     
%     answer = inputdlg({[ 'Resonant \sigma_0 (\mu m^2)' cr ...
%         'Rb: 0.2907 for \sigma^+, 0.1357 for random ' cr ...
%         'Li: 0.215 for \sigma^+, 0.100 for random' ], ...
%         'Linewidth (MHz) (Rb: 6.065, Li: 5.87)',...
%         'Detuning (MHz)',...
%         'I/I_{sat}'},...
%         'Cross-section calculator',1, ...
%         {num2str(sigma0),num2str(linewidth),...
%         num2str(delta),...
%         num2str(sat)},options);
% 
%     if length(answer)==4
%         sigma0 = str2double(answer{1});
%         linewidth = str2double(answer{2});
%         delta = str2double(answer{3});
%         sat = str2double(answer{4});
%         if isreal(sigma0) && (sigma0 < Inf) && isreal(linewidth) && (linewidth < Inf)
%             if not(isreal(sat) && (sat < Inf))
%                 sat = 0;
%             end
%             if not(isreal(delta) && (delta < Inf))
%                 delta = 0;
%             end
%             camera.detuning = delta/linewidth;
%             camera.crosssection = sigma0;
%             camera.linewidth = 2*pi*linewidth;
%             if (sat > 0)
%                 camera.scatteringrate = (pi*linewidth)*sat/(1 + 4*(delta/linewidth)^2 + sat);
%             else
%                 camera.scatteringrate = pi*linewidth;
%             end
% 
%             camera
%         end
% 
%         handles = redraw(handles);
%     end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
global camera atom atomdata edcamroot
    save([edcamroot 'currentcamera.mat'],'camera');
    save([edcamroot 'atomdata.mat'],'atom','atomdata');
    delete(hObject);


% --- Executes on selection change in popupmenu_atomdata.
function popupmenu_atomdata_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_atomdata (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu_atomdata contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_atomdata
global atomdata atom
    atom = atomdata(get(hObject,'Value'));
    handles = redraw(handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_atomdata_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_atomdata (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_scatteringrate_Callback(hObject, eventdata, handles)
% hObject    handle to edit_scatteringrate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_scatteringrate as text
%        str2double(get(hObject,'String')) returns contents of edit_scatteringrate as a double
global atom
    scatteringrate = str2double(get(hObject,'String'));
    if (isempty(get(hObject,'String'))) || isnan(scatteringrate)
        set(hObject,'String',atom.scatteringrate);
    else
        atom.scatteringrate = scatteringrate;
    end
    guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function edit_scatteringrate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_scatteringrate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


