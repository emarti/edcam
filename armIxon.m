function pass = armIxon()
%TAKEPICTURE Summary of this function goes here
%   Detailed explanation goes here
% variable declarations
    
    currentpath = pwd;
    installpath = fullfile(matlabroot,'toolbox','Andor','Camera Files');
    cd (installpath);
[ret,gstatus]=AndorGetStatus;
if gstatus == 20073
    pass = 1;
    cd(currentpath);
else
    status = zeros(1,1);
    head_model = '....................';
    xpixels = zeros(1,1);
    ypixels = zeros(1,1);
    minTemp = zeros(1,1);
    maxTemp = zeros(1,1);
    currentTemp = zeros(1,1);
    
    %temperature and fan settings

    cooler = 0; %1 = ON, 0 = OFF
    setTemp = 20; %only used when cooler is on
%     setTemp = 20;   
    if cooler
        FanMode = 0; %  0 = HIGH, 1 = LOW, 2 = OFF.
    else
        FanMode = 2; % 0 = HIGH, 1 = LOW, 2 = OFF.
    end
    
    PCB = zeros(1,1);
    Flex = zeros(1,1);
    dummy1 = zeros(1,1);
    dummy2 = zeros(1,1);
    dummy3 = zeros(1,1);
    dummy4 = zeros(1,1);
    numberVSSpeeds = zeros(1,1);
    VSSpeedIndex = zeros(1,1);
    VSSpeed = zeros(1,1);
    eprom = zeros(1,1);
    cofFile = zeros(1,1);
    vxdRev = zeros(1,1);
    vxdVer = zeros(1,1);
    dllRev = zeros(1,1);
    dllVer = zeros(1,1);
    numberHSSpeeds = zeros(1,1);
    HSSpeedIndex = zeros(1,1);
    HSSpeed = zeros(1,1);
    AcquisitionMode = 3;% accumulate scan
    ReadMode = 4;% Image
    ShutterType = 1;% TTL high
    ShutterMode = 0;% open
    ClosingTime = 100;% ms
    OpeningTime = 300;% ms
    ExposureTime = 0;% setting zero will result in minimum possible (non-zero) exposure time
    TriggerMode = 1;% 0 = internal, 1 = external
    AccCycleTime = 0;
    NumAcc = 1;
    NumKin = 1;
    KinCycleTime = 0;
    validExpTime = zeros(1,1);
    validAccTime = zeros(1,1);
    validKinTime = zeros(1,1);

    head_model = 'anyway';
    addpath(fullfile(matlabroot,'toolbox','Andor'))
    path = '';


    disp('Start Andor Camera Control')
    % init system
    installpath = fullfile(matlabroot,'toolbox','Andor','Camera Files');
    cd (installpath);
    disp('AndorInitialize ---------------------------------------');
    returnCode=AndorInitialize(path);
    path
    returnCode
    
    [ret,head_model]=GetHeadModel;
    [ret,xpixels,ypixels]=GetDetector;
    [ret]=SetImage(1,1,1,xpixels,1,ypixels);
    data = zeros(xpixels*ypixels,1);
    [ret,PCB,Flex,dummy1,dummy2,dummy3,dummy4]=GetHardwareVersion;
    if ret ~= 20002
        pass = 0;
    else
        [ret,numberVSSpeeds]=GetNumberVSSpeeds
        [ret,VSSpeed]=GetVSSpeed(VSSpeedIndex)
        [ret,eprom,cofFile,vxdRev,vxdVer,dllRev,dllVer]=GetSoftwareVersion
        [ret,numberHSSpeeds]=GetNumberHSSpeeds(0,0)
        [ret,HSSpeed]=GetHSSpeed(0,0,HSSpeedIndex)

        [ret]=SetCoolerMode(1);
        [ret]=SetAcquisitionMode(AcquisitionMode);
        [ret]=SetReadMode(ReadMode);
        [ret]=SetShutter(ShutterType,ShutterMode,ClosingTime,OpeningTime);
        [ret]=SetExposureTime(ExposureTime);
        [ret]=SetTriggerMode(TriggerMode);
        [ret]=SetAccumulationCycleTime(AccCycleTime);
        [ret]=SetNumberAccumulations(NumAcc);
        [ret]=SetNumberKinetics(NumKin);
        [ret]=SetKineticCycleTime(KinCycleTime);
        [ret,validExpTime,validAccTime,validKinTime]=GetAcquisitionTimings;
        % [ret]=SetHSSpeed(1,numberHSSpeeds-1);
        [ret]=SetADChannel(0);
        [ret]=SetHSSpeed(0,0); % 0,x: x=0 for 35MHz, x=1 for 27MHz, x=2 for 13MHz
        [ret]=SetPreAmpGain(1); % CHANGE ME!!!! NORMALLY 2
        [ret]=SetVSSpeed(2);

        % Temperature Stuff
        [ret,minTemp,maxTemp]=GetTemperatureRange;
        cd(currentpath)

        if cooler
            [ret]=SetTemperature(setTemp);
            [ret]=SetFanMode(FanMode); 
            [ret]=CoolerON;
            pause(3);
            [ret,currentTemp]=GetTemperature;
            sprintf('Current temperature is %f deg',currentTemp)
            while(abs(currentTemp - setTemp) > 1)
                pause(10.0);
                [ret,currentTemp]=GetTemperature;
                sprintf('Current temperature is %f deg',currentTemp)
            end
            
        else
            [ret]=SetFanMode(FanMode); 
            [ret]=CoolerOFF;
            [ret,currentTemp]=GetTemperature;
            sprintf('Current temperature is %f deg',currentTemp)
        end
        

        [ret,gstatus]=AndorGetStatus
        if gstatus == 20073
            pass = 1;
        else
            pass = 0;
        end
    end
end